from pyspark.sql.functions import col, when, lit, concat, countDistinct, coalesce, udf, coalesce, collect_list, avg, rand
from pyspark.sql.types import DoubleType, ArrayType, StructType, StructField, IntegerType, StringType, FloatType
import spark_deep_learning.config as cnf



def get_viewability_for_single_ad(df):
    base_df = df.select('url', 'event_type', 'page_view_id', 'ad_index', 'is_lazy_loaded', col('offset_top_d').alias('ad_location'), 
                      
                        coalesce(col('refresh_count'), lit(0)).alias('refresh_count')) \
        .withColumn('is_lazy_loaded', when(col('is_lazy_loaded') == True, lit(1)).otherwise(lit(0)).cast('double')) \
        .filter(df['fold'].isNotNull()).cache()\




  
    imps = base_df.filter(base_df['event_type'] == 'ad_impression').drop('event_type').distinct()
    views = base_df.filter(base_df['event_type'] == 'viewed_impression') \
        .select(col('page_view_id').alias('views_page_view_id'), col('ad_index').alias('views_ad_index'),
                col('refresh_count').alias('views_refresh_count')) \
        .distinct()

    joined_df= imps.join(views, [imps['page_view_id'] == views['views_page_view_id'],
                             imps['ad_index'] == views['views_ad_index'],
                             imps['refresh_count'] == views['views_refresh_count']
                             ], how='left_outer') \
        .select(imps['url'],'is_lazy_loaded', 'ad_location',
                when(col('refresh_count') == 0, 0).otherwise(1).alias('is_refreshed'),
                when(concat('views_page_view_id', 'views_ad_index').isNull(), 0).otherwise(1).alias('is_viewed'))

    return joined_df.orderBy(rand()).limit(cnf.num_of_imps_to_sample)





