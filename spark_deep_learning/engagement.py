from pyspark.sql.functions import col, when, lit, coalesce, avg, count, sum, udf, max, countDistinct
from pyspark.sql import functions as F
from pyspark.sql.types import DoubleType, FloatType
from spark_deep_learning import config as cnf





def get_engagement_per_url(df):
    filtered = df \
        .select('url', 'page_view_id', 'depth_percentage', 'time_on_page', 'time_delta',  'velocity', 'load_time', 'mc_size') \
        .filter(df['is_benchmark_pageview'] == False) \
        .filter(df['depth_percentage'].between(1, 100)).cache()

    velocity_df=filtered.groupBy('url')\
        .agg(
             avg(when(col('velocity')>0, col('velocity'))).alias('avg_velocity_url')
           )


    maxes_df = maxes(filtered)



    with_maxes = velocity_df.join(maxes_df, 'url', 'inner')
    url_df = with_maxes.select('url', 'avg_velocity_url', 'avg_scroll_depth_url', 'avg_time_on_page_url', 'avg_load_time_url', 'mc_size_url').filter(with_maxes['pvs']>cnf.pv_cap_for_urls)

            

    return url_df



def maxes(df):
    max_per_pv=df.groupBy('url','page_view_id').agg(
            F.max('time_on_page').alias('time_on_page'),
            F.max('depth_percentage').alias('depth_percentage'),
            F.max('load_time').alias('load_time'),
           (F.sum(df.mc_size*df.time_delta)/F.sum('time_delta')).alias('mc_size') \


        )


    avg_per_url=max_per_pv.groupBy('url').agg(
             avg('depth_percentage').alias('avg_scroll_depth_url'),
             avg('time_on_page').alias('avg_time_on_page_url'),
             avg('load_time').alias('avg_load_time_url'),
             avg('mc_size').alias('mc_size_url'),
             countDistinct('page_view_id').alias('pvs'))


    return avg_per_url.drop('time_on_page', 'depth_percentage', 'load_time', 'mc_size', 'time_delta', 'page_view_id')
