from datetime import timedelta, datetime

_parquet_bucket_name = 'browsi-events-parquet-production'
_json_bucket_name = 'browsi-events-raw-production'


def _getPartitions(base_date, num_days_back):
    base_date=datetime.strptime(base_date, '%Y-%m-%d')
    date_list = [base_date - timedelta(days=x) for x in range(0, num_days_back)]
    return list(map(lambda date: 'year={:04d}/month={:02d}/day={:02d}/*'.format(date.year, date.month, date.day), date_list))


def _fetchParquetData(session, topic, base_date, num_days_back):
    partitions = _getPartitions(base_date, num_days_back)
    file_paths = list(map(lambda partition: 's3a://{}/{}/{}'.format(_parquet_bucket_name, topic, partition), partitions))
    return session.read.parquet(*file_paths)


def _fetchJsonData(session, topic, base_date, num_days_back):
    partitions = _getPartitions(base_date, num_days_back)
    file_paths = list(map(lambda partition: 's3a://{}/{}/{}'.format(_json_bucket_name, topic, partition), partitions))
    return session.read.json(file_paths)


def fetchEngagementData(session, base_date, num_days_back):
    return _fetchParquetData(session, 'engagement_parquet', base_date, num_days_back)


def fetchDemandData(session, base_date, num_days_back):
    return _fetchParquetData(session, 'demand_parquet', base_date, num_days_back)


def fetchPageStructure(session, base_date, num_days_back):
    return _fetchJsonData(session, 'browsi_general_page_structure', base_date, num_days_back)