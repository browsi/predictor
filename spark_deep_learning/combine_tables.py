import random
from datetime import datetime

from pyspark.sql.functions import col, udf

from pyspark.sql.types import BooleanType



def joins(ad_viewability,url_engage):
    return  ad_viewability.join(url_engage, ['url'], how='inner')



@udf(returnType=BooleanType())

def is_homepage(url):
    from urlparse import urlparse
    #from urllib.parse import urlparse
    try:
        length = len(urlparse(url).path)
    except:
        length=1

    return length > 1


def run(engagement_df, demand_df):
    from spark_deep_learning import engagement
    from spark_deep_learning import viewability


    engagement_data = engagement_df.filter(is_homepage(col('url'))).cache()
    demand_data = demand_df.filter(is_homepage(col('url'))).cache()



    ad_viewability = viewability.get_viewability_for_single_ad(demand_data)
    url_engage = engagement.get_engagement_per_url(engagement_data.alias('engagement2'))
    output = joins( ad_viewability, url_engage)



    return output.select('url',  'ad_location', 'is_lazy_loaded', 'is_refreshed', 'avg_scroll_depth_url', 'avg_time_on_page_url', 'avg_load_time_url',
                          'avg_velocity_url', 'mc_size_url',
                         'is_viewed')



