from spark_deep_learning import combine_tables
from spark_deep_learning import s3_data_fetch
from spark_deep_learning.config import cluster_vars


def getSparkSession():
    from pyspark.sql import SparkSession
    from pyspark.context import SparkContext
    # build a SparkSession object that contains information about your application
    # the appName parameter is a name for your application to show on the cluster UI
    # most options should be set at runtime when launching with spark-submit
    session = SparkSession.builder.appName("modelGenerator").getOrCreate()
    session._jsc.hadoopConfiguration().set("fs.s3a.awsAccessKeyId", "AKIAIBXXAWHRRBO2KHYA")
    session._jsc.hadoopConfiguration().set("fs.s3a.awsSecretAccessKey",
                                           "qC0EWqrqfSfodb9LnNB2WERKyPFk7JIRpcwVsd5R")

    return session

def save_data_frame(df,  base_path):
    df.repartition(10).write.csv(path="{}/result_set/".format(base_path), header=True, mode='overwrite', quote='')

if __name__ == '__main__':


    base_date=cluster_vars['base_date']
    num_days_back=cluster_vars['num_days_back']
    fraction=cluster_vars['fraction']
    spark_session = getSparkSession()
    configs = spark_session.sparkContext.getConf().getAll()
    for x in configs:
        print(x)
    engagement_events = s3_data_fetch.fetchEngagementData(spark_session, base_date, num_days_back)
    demand_events = s3_data_fetch.fetchDemandData(spark_session, base_date, num_days_back)


    stage1 = combine_tables.run(engagement_events, demand_events)
    stage2 = stage1.randomSplit([fraction, 1.0 - fraction], 24)[0]

    print('***************************')
    print('started writing')
    save_data_frame(stage2,  's3a://browsi-predictor-model-dev/automation/data/hammer_deep')
    print('Great success')

