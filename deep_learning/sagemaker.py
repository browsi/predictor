

import tensorflow as tf




def my_input_fn(main_file_path, mode, perform_shuffle=True, repeat_count=1, batch_size=500, buffer_size=1000):
    # This function gets the main path to the file and the mode and returns one batch of features and labels
    #it is called by the main input function untill it covers all data



    def decode_csv(line):
        parsed_line = tf.decode_csv(line, [["NA"],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0] ,[0]])
        label = parsed_line[-1:]  # Last element is the label
        del parsed_line[-1]  # Delete last element
        features = parsed_line[1:]  # Everything (but last element) are the features
        return features,  tf.one_hot(tf.squeeze(label),2)



    def get_path(dir, mode): #input: main dir of all data, output: the path to train or validation files
        import os
        if mode == tf.estimator.ModeKeys.TRAIN:
            return os.path.join(dir, 'train')

        elif mode == tf.estimator.ModeKeys.EVAL:
            return os.path.join(dir, 'test')
        else:
            raise ValueError('Invalid mode: %s' % mode)

    wanted_file_path=get_path(main_file_path,mode) #get path to train files or validation files
    dataset = (tf.data.TextLineDataset(wanted_file_path) # Read text file. it is a representation of an input pipeline. the data is not really read into memory.
    .map(decode_csv)) # Transform each elem by applying decode_csv fn
    if perform_shuffle:
       # Randomizes input using a window of 256 elements (read into memory)
        dataset = dataset.shuffle(buffer_size=buffer_size)
    dataset = dataset.repeat(repeat_count) # Repeats dataset this # times
    dataset = dataset.batch(batch_size)  # Batch size to use
    iterator = dataset.make_one_shot_iterator()
    batch_features, batch_labels = iterator.get_next() #get batch
    return {'features': batch_features}, batch_labels



def train_input_fn(training_dir, hyperparameters):
    return  my_input_fn(main_file_path=training_dir, mode=tf.estimator.ModeKeys.TRAIN, perform_shuffle=True, repeat_count=1,
                        batch_size=hyperparameters['batch_size'], buffer_size=1000 )


def eval_input_fn(training_dir, hyperparameters):
    return  my_input_fn(main_file_path=training_dir, mode=tf.estimator.ModeKeys.TRAIN, perform_shuffle=True, repeat_count=1,
                        batch_size=hyperparameters['batch_size'], buffer_size=1000 )





def serving_input_fn():
    feature_spec = {'inputs': tf.FixedLenFeature(dtype=tf.float32, shape=[8])}
    return tf.estimator.export.build_parsing_serving_input_receiver_fn(feature_spec)()


def model_fn(features, labels, mode, hyperparameters):

    global_step = tf.train.get_global_step()  # initiate global counter of training steps that will be shared among all instances

    def init_weights(shape, name):
        """ Weight initialization """
        initializer = tf.contrib.layers.xavier_initializer(uniform=False)
        return tf.Variable(initializer(shape), name=name)


    def init_bias(shape, name):
        """ Weight initialization """
        bias = tf.zeros(shape)
        return tf.Variable(bias, name=name)


    N_0 = features.shape[1]  # size of "zero layer" = number of features
    N_1 = 154
    N_2 = 256
    N_3 = 64
    N_4 = labels.shape[1]  # "size of output layer=2 since its a binary classification problem

    with tf.name_scope('weights'):

        w_1 = init_weights((N_1, N_0), "w1")
        w_2 = init_weights((N_2, N_1), "w2")
        w_3 = init_weights((N_3, N_2), "w3")
        w_4 = init_weights((N_4, N_3), "w3")

    with tf.name_scope('biases'):

        b_1 = init_bias((N_1, 1), "b1")
        b_2 = init_bias((N_2, 1), "b2")
        b_3 = init_bias((N_3, 1), "b3")
        b_4 = init_bias((N_4, 1), "b3")

    use_reg = hyperparameters['use_reg']
    reg_beta = hyperparameters['reg_beta']
    beta1 = hyperparameters['beta1']
    beta2 = hyperparameters['beta2']
    base_learning_rate = hyperparameters["base_learning_rate"]
    use_learning_decay = hyperparameters['use_learning_decay']
    decay_steps = hyperparameters['decay_steps']  # after how many steps use the dacay
    decay_rate = hyperparameters['decay_rate']  # in what factor should we decrease the learning rate
    use_drop_out = hyperparameters['use_droput']
    drop_out_rate = hyperparameters['drop_out_rate']
    use_batch_norm=hyperparameters['use_batch_norm']



    inputs=features['features']

    z_1 = tf.matmul(inputs, tf.transpose(w_1), name='Z1')

    if use_batch_norm:
        z_1 = tf.layers.batch_normalization (inputs=z_1, training=mode == tf.estimator.ModeKeys.TRAIN)
    else:
        z_1 = tf.add(z_1, tf.transpose(b_1))

    a_1 = tf.nn.tanh(z_1)
    if use_drop_out.dropout:
        a_1 = tf.layers.dropout(inputs=a_1, rate=drop_out_rate, training=mode == tf.estimator.ModeKeys.TRAIN)


    z_2 = tf.matmul(a_1, tf.transpose(w_2), name='Z2')
    if use_batch_norm:
        z_2 = tf.layers.batch_normalization(inputs=z_2, training=mode == tf.estimator.ModeKeys.TRAIN)
    else:
        z_2 = tf.add(z_2, tf.transpose(b_2))

    a_2 = tf.nn.tanh(z_2)
    if use_drop_out.dropout:
        a_2 = tf.layers.dropout(inputs=a_2, rate=drop_out_rate, training=mode == tf.estimator.ModeKeys.TRAIN)

    z_3 = tf.matmul(a_2, tf.transpose(w_3), name='Z3')
    if use_batch_norm:
        z_3 = tf.layers.batch_normalization(inputs=z_3, training=mode == tf.estimator.ModeKeys.TRAIN)
    else:
        z_3 = tf.add(z_3, tf.transpose(b_3))

    a_3 = tf.nn.tanh(z_3)
    if use_drop_out.dropout:
        a_3 = tf.layers.dropout(inputs=a_3, rate=drop_out_rate, training=mode == tf.estimator.ModeKeys.TRAIN)

    z_4 = tf.matmul(a_3, tf.transpose(w_4), name='Yhat')
    if use_batch_norm:
        z_4  = tf.layers.batch_normalization (inputs=z_4, training=mode == tf.estimator.ModeKeys.TRAIN)
    else:
        z_4 = tf.add(z_4, tf.transpose(b_4))

    probas = tf.nn.sigmoid(z_4, name='get_prediction')




    if use_learning_decay:
        learning_rate = tf.train.exponential_decay(base_learning_rate, global_step,
                                                   decay_steps, decay_rate, staircase=True)
    else:
        learning_rate=base_learning_rate



    if use_reg:
        reg = reg_beta * (tf.nn.l2_loss(w_1) + tf.nn.l2_loss(w_2)+ tf.nn.l2_loss(w_3))
    else:
        reg = 0


    cost_function = tf.reduce_mean(labels * -tf.log(probas) + (1 - y) * -tf.log(1 - probas) + reg)
    optimizer= tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=beta1,
                           beta2=beta2)
    train_op = optimizer.minimize(
        loss=cost_function, global_step=global_step)

    predict = tf.argmax(probas, axis=1)


    # Provide an estimator spec for `ModeKeys.PREDICT`.
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions={"probas": probas})

    eval_metric_ops = {
        'accuracy': tf.metrics.accuracy(labels, predict),
        'log_loss': cost_function
    }

    # Provide an estimator spec for `ModeKeys.EVAL` and `ModeKeys.TRAIN` modes.
    return tf.estimator.EstimatorSpec(
        mode=mode,
        loss=cost_function,
        train_op=train_op,
        eval_metric_ops=eval_metric_ops)







