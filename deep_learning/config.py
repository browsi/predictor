# coding: utf-8

# So far bes results with user data, balanced (combined), no regularization, adam optimizer, no pre-standarize, no learning decay, Xavier init, batchnorm true with size 25600

X_columns=['ad_location', 'is_lazy_loaded', 'is_refreshed', 'avg_scroll_depth_url', 'avg_time_on_page_url', 'avg_load_time_url', 'avg_velocity_url', 'avg_mc_size']
target='is_viewed'
path_to_data='./sltrib.csv'
unwanted_columns=['url']
target='is_viewed'
number_of_classes=2

sample=0.99  #between 0 and 1
write_feature_order=False
pre_standarize=False #We have batch normalization for that
balance=False
balance_method='combined'
percent_for_train=0.9

early_stop=100  #for no early stop put 100

early_stop_logloss=0.02

regularize=False
regularization_beta=0.01
optimizer='adam'
optimizer_dict={'adam': {'learning_rate':0.0001, 'beta1':0.9, 'beta2':0.99},
                'momentum': {'learning_rate':0.0001, 'momentum':0.9},
                'RMS':     {'learning_rate':0.0001, 'decay':0.99 },
                'gd':      {'learning_rate':0.0001}
                }

dropout=True
keep_prob=0.8
epoches=1000


learning_decay=True
alpha_0    = 0.0001
decay_rate = 0.9

xavier=False


batch_size=256


batch_norm=False
bn_decay=0.999
