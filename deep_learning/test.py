# coding: utf-8
import boto3
import pandas as pd
import tensorflow as tf
import numpy as np
import config
from Preprocessing import get_data
import shutil
import os
import time

def init_weights(shape, name):
    """ Weight initialization """
    if config.xavier:
        initializer = tf.contrib.layers.xavier_initializer(uniform=False)
        return tf.Variable(initializer(shape), name=name)

    else:
        initializer = tf.contrib.layers.variance_scaling_initializer(uniform=False)
        return tf.Variable(initializer(shape), name=name)



def init_bias(shape,name):
    """ Weight initialization """
    bias = tf.zeros(shape)
    return tf.Variable(bias, name=name)



def forwardprop(X, w_1, w_2, w_3,w_4, b_1, b_2, b_3,b_4,is_train):
    """
    Forward-propagation.
    IMPORTANT: yhat is not softmax since TensorFlow's softmax_cross_entropy_with_logits() does that internally.
    """


    z_1=tf.matmul(X, tf.transpose(w_1), name='Z1')

    if config.batch_norm:
        z_1=BatchNormalization(z_1, is_train)
    else:
        z_1=tf.add(z_1,tf.transpose(b_1))

    a_1=tf.nn.tanh(z_1)
    if config.dropout:
        a_1=tf.contrib.layers.dropout(a_1, config.keep_prob)


    z_2=tf.matmul(a_1,tf.transpose(w_2), name='Z2')
    if config.batch_norm:
        z_2=BatchNormalization(z_2, is_train)
    else:
        z_2=tf.add(z_2,tf.transpose(b_2))

    a_2=tf.nn.tanh(z_2)
    if config.dropout:
        a_2=tf.contrib.layers.dropout(a_2, config.keep_prob)



    z_3=tf.matmul(a_2,tf.transpose(w_3), name='Z3')
    if config.batch_norm:
        z_3=BatchNormalization(z_3, is_train)
    else:
        z_3=tf.add(z_3,tf.transpose(b_3))

    a_3=tf.nn.tanh(z_3)
    if config.dropout:
        a_3=tf.contrib.layers.dropout(a_3, config.keep_prob)

    z_4 = tf.matmul(a_3, tf.transpose(w_4), name='Yhat')
    if config.batch_norm:
        z_4 = BatchNormalization(z_4, is_train)
    else:
        z_4 = tf.add(z_4, tf.transpose(b_4))


    
    # Z_4 is a vector that has N rows (N=samples) and 2 columns and looks like [[20,32.5],[-3,45]..]
    # after activating tf.nn.softmax() on Z_4 (y_hat) we get something like this [[0.2,0.8], [0.1,0.9]..]
    # We activate the cost function on Z_4 (y_hat) since the softmax conversion is embedded insided the loss function "tf.nn.weighted_cross_entropy_with_logits(Y_true i.e. [[0,1],[1,0]], 
    #      yhat   i.e  [[20,50],  [-10,2]])"
    # we get the y_predict by activating the argmax on yhat (which will give us 0 if the 0th argument is the highest and 1 if the 1th argument is the highest) 
 
    yhat=z_4
    probas=tf.nn.softmax(yhat,  name='get_prediction')

    return yhat,   my_tf_round(probas,2)

def my_tf_round(x, decimals = 0):
    multiplier = tf.constant(10**decimals, dtype=x.dtype)
    return tf.round(x * multiplier) / multiplier

def get_optimizer(cost_function, learning_rate):
    import tensorflow as tf
    from config import optimizer_dict
    from config import optimizer
    optimizer_dict=optimizer_dict[optimizer]
    if learning_rate is None:
        learning_rate = optimizer_dict['learning_rate']
    if optimizer=='adam':
        return tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=optimizer_dict['beta1'],beta2=optimizer_dict['beta2']).minimize(cost_function)
    elif optimizer=='RMS':
        return tf.train.RMSPropOptimizer(learning_rate=learning_rate, decay=optimizer_dict['decay']).minimize(cost_function)
    elif optimizer=='momentum':
        return tf.train.MomentumOptimizer(learning_rate=learning_rate,momentum=optimizer_dict['momentum']).minimize(cost_function)
    else:
        return tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(cost_function)


def BatchNormalization(Z, is_training):
    """ Batch normalization for TensorFlow r0.9
    """
    return tf.cond(is_training,
            lambda: tf.contrib.layers.batch_norm(Z, is_training=True,
                               center=True, scale=True,  updates_collections=None, decay=config.bn_decay),
            lambda: tf.contrib.layers.batch_norm(Z, is_training=False,
                               updates_collections=None, scale=True, center=True, decay=config.bn_decay), name='is_training_condition')



with tf.device("/gpu:0"):

    X_train, Y_train, X_test,Y_test = get_data()



m   = X_train.shape[0]   # number of samples
N_0 = X_train.shape[1]   # size of "zero layer" = number of features
N_1 = 154
N_2 = 256
N_3 = 64
N_4 = Y_train.shape[1]   #"size of output layer=2 since its a binary classification problem

with tf.name_scope('weights'):

    w_1 = init_weights((N_1,N_0),"w1")
    w_2 = init_weights((N_2, N_1), "w2")
    w_3 = init_weights((N_3, N_2), "w3")
    w_4 = init_weights((N_4, N_3), "w3")

with tf.name_scope('biases'):

    b_1 = init_bias((N_1,1), "b1")
    b_2 = init_bias((N_2,1), "b2")
    b_3 = init_bias((N_3,1), "b3")
    b_4 = init_bias((N_4,1), "b3")

print('N0 is ', N_0)
print('N1 is ', N_1)
print('N2 is ', N_2)
print('w1 is ', w_1)
print('w2 is ', w_2)

# Symbols
X = tf.placeholder("float", shape=[None, N_0], name="X") #initialize place holder for undifined number of  X vectors in the size of the input. Its is undifined since during the training it gets 1 X vector each iteration (mini batch) and during test, it test on the entire X_test
y = tf.placeholder("float", shape=[None, N_4], name="Y") #initialize place holder for undifined number of  Y values
is_train=tf.placeholder(bool, shape=[], name="is_train")

# Forward propagation
yhat,probas = forwardprop(X, w_1, w_2, w_3,w_4, b_1, b_2, b_3,b_4, is_train)


predict = tf.argmax(yhat, axis=1) # retrieve the index of the max probability across all labels

logloss=tf.losses.log_loss(labels=Y_test, predictions=probas)


# Backward propagation

if config.regularize:
    reg=config.regularization_beta*(tf.nn.l2_loss(w_1)+tf.nn.l2_loss(w_2))
else:
    reg=0



learning_rate=None #Initialize as None, is learning rate decay is selected--> this value will be updated each epoch, if not it will get values from the dict

cost = tf.reduce_mean(tf.nn.weighted_cross_entropy_with_logits(targets=y, logits=yhat, pos_weight=10)+reg)


update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)


num_of_batches=int(X_train.shape[0]/config.batch_size)

with tf.control_dependencies(update_ops):
    updates=get_optimizer(cost, learning_rate)

sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
init = tf.global_variables_initializer()
sess.run(init)
epoch=1
break_flag=False
while epoch<=config.epoches and break_flag==False:
    # Train on mini batches
    if config.learning_decay:
        learning_rate=(1/(1+config.decay_rate*epoch))*config.alpha_0
    for batch in range(num_of_batches):
        offset = (batch * config.batch_size) % (X_train.shape[0] - config.batch_size)
        # Create batch data and labels
        


        _,yhatt ,probs_= sess.run([updates,yhat, probas],
                                                 feed_dict={X: X_train[offset: (offset + config.batch_size) ,:],
                                                            y: Y_train[offset: (offset + config.batch_size) ,:],
                                                            is_train: True})

        
        
    train_accuracy = np.mean(np.argmax(Y_train, axis=1) ==
                             sess.run(predict, feed_dict={X: X_train, y: Y_train, is_train:True})) #run predict-->for this it calculates yhat with forward prop given the current X. it uses the currect Y to check the predictions
    test_accuracy = np.mean(np.argmax(Y_test, axis=1) ==
                            sess.run(predict, feed_dict={X: X_test, y: Y_test, is_train: False}))
    
    logloss_=sess.run(logloss, feed_dict={X: X_test, y: Y_test, is_train: False})
    print('logg loss is:' , logloss_)
    
    print("Epoch = %d, train accuracy = %.2f%%, test accuracy = %.2f%%"
          % (epoch + 1, 100. * train_accuracy, 100. * test_accuracy))
    epoch=epoch+1
    if 100.*test_accuracy>=config.early_stop or logloss_<config.early_stop_logloss:
        break_flag=True


Y_true=np.argmax(Y_test, axis=1)
probas_=sess.run(probas, feed_dict={X: X_test, y: Y_test, is_train: False})
output=np.column_stack((probas_[:,1],Y_true))


np.savetxt('deep_learning_test_result_10k_epochs.csv', output, delimiter=",")
hist,bin_edges= np.histogram(probas_[:,1],10)

print('*******************')
print('Histogram of result set')
print(hist)
print(bin_edges)
print('*******************')



if os.path.exists('./tf_model'):
    print('deleting previous models from local fs')
    shutil.rmtree('./tf_model')



builder = tf.saved_model.builder.SavedModelBuilder('./tf_model')

tensor_info_x = tf.saved_model.utils.build_tensor_info(X)
tensor_info_y = tf.saved_model.utils.build_tensor_info(probas)
tensor_info_is_train = tf.saved_model.utils.build_tensor_info(is_train)

prediction_signature = (
    tf.saved_model.signature_def_utils.build_signature_def(
        inputs={'X': tensor_info_x, 'is_train': tensor_info_is_train},
        outputs={'Y': tensor_info_y},
        method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME))

builder.add_meta_graph_and_variables(
    sess,
    [tf.saved_model.tag_constants.SERVING],
    signature_def_map={tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: prediction_signature},
)

builder.save()
print('****************************')
print('Model saved- session closed')
print('****************************')

sess.close()



result_path=os.getcwd()+'/tf_model'

file_lst=[]


#get full גpath of all of the files in the saved model
for root, dirs, files in os.walk(result_path , topdown=False):
    for name in files:
        file_lst.append(os.path.join(root, name))








#push files to S3

s3 = boto3.resource('s3')
BUCKET = "browsi-predictor-model-dev"
now = timestamp = int(time.time())
for file in file_lst:
    parent_path=os.path.abspath(os.path.join(file, os.pardir))
    if parent_path==result_path:
        destination='tensorflow/url/'+str(now)+'/'+os.path.basename(file)
    else:
        destination = 'tensorflow/url/' + str(now) + '/' + os.path.basename(parent_path)+'/'+os.path.basename(file)

    s3.Bucket(BUCKET).upload_file(file, destination )
    print('uploading file ', file, 'to   ', destination)

exit()




