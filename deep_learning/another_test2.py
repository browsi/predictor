
import logging
import tensorflow as tf


def train_input_fn(training_dir, hyperparameters):
    return  input_pipeline(training_dir=training_dir,
                        batch_size=hyperparameters['batch_size'])


def eval_input_fn(training_dir, hyperparameters):
    return  input_pipeline(training_dir=training_dir,
                        batch_size=hyperparameters['batch_size'])



def read_from_csv(filename_queue):
    reader = tf.TextLineReader(skip_header_lines=1)   # A Reader that outputs the lines of a file, delimiter= /n
    _, csv_row = reader.read(filename_queue)     #Returns the next record (key, value) pair produced by a reader.
    record_defaults = [["NA"],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0],[0.0] ,[0.0]]
    _,col2,col3, col4, col5, col6, col7,col8, col9, lab = tf.decode_csv(csv_row, record_defaults=record_defaults) # Convert CSV records to tensors
    features = tf.stack([col2,col3, col4, col5, col6, col7, col8, col9]) #pack aling the first dimension i.e. columns
    features.set_shape([8])
    label = lab
    return features, label

def filenames(data_dir):
    import os
    files_paths = map(lambda x: os.path.join(data_dir, x), os.listdir(data_dir))
    return files_paths

def input_pipeline(training_dir, batch_size, num_epochs=1):
    file_paths=filenames(training_dir)
    filename_queue = tf.train.string_input_producer(file_paths, num_epochs=num_epochs, shuffle=True) #returns a queue with the Output strings (e.g. filenames),
                                                                                               # and also adds a QueueRunner to the Graph's collection
    print(file_paths)                                                                                               # num_epochs defines how many times the queue produces each string, if None then it is an infinate loop
                                                                                               # before generating an OutOfRange error
    example, label = read_from_csv(filename_queue)
    min_after_dequeue = 10
    capacity = min_after_dequeue + 3 * batch_size
    example_batch, label_batch = tf.train.shuffle_batch(tensors=
      [example, label], batch_size=batch_size, capacity=capacity,
      min_after_dequeue=min_after_dequeue, num_threads=2, allow_smaller_final_batch=True)
  # Creates batches by randomly shuffling tensors. It create a shuffling queue into which the [example,label] tensors are enqued
  # It also adds a dequey_many operation to create batches from the queue, and ads a QueueRunner to the collection
  # Capacity=maximum number of elements in in the queue.
  # min_after_dequeue: Minimum number elements in the queue after a dequeue, used to ensure a level of mixing of elements.
    return {'inputs': example_batch}, label_batch




def serving_input_fn(hyperparameters):
    feature_spec = {'inputs': tf.FixedLenFeature(dtype=tf.float32, shape=[8])}
    return tf.estimator.export.build_parsing_serving_input_receiver_fn(feature_spec)()


def model_fn(features, labels, mode, hyperparameters):

    #print_op=tf.Print('we are here!!')


    global_step = tf.train.get_global_step()  # initiate global counter of training steps that will be shared among all instances

    def init_weights(shape, name):
        """ Weight initialization """
        initializer = tf.contrib.layers.xavier_initializer(uniform=False)
        return tf.Variable(initializer(shape), name=name)


    def init_bias(shape, name):
        """ Weight initialization """
        bias = tf.zeros(shape)
        return tf.Variable(bias, name=name)


    N_0 = 8
        #features.shape[1]  # size of "zero layer" = number of features
    N_1 = 154
    N_2 = 256
    N_3 = 64
    N_4 = 1
        #labels.shape[1]  # "size of output layer=2 since its a binary classification problem

    with tf.name_scope('weights'):

        w_1 = init_weights((N_1, N_0), "w1")
        w_2 = init_weights((N_2, N_1), "w2")
        w_3 = init_weights((N_3, N_2), "w3")
        w_4 = init_weights((N_4, N_3), "w3")

    with tf.name_scope('biases'):

        b_1 = init_bias((N_1, 1), "b1")
        b_2 = init_bias((N_2, 1), "b2")
        b_3 = init_bias((N_3, 1), "b3")
        b_4 = init_bias((N_4, 1), "b3")

    use_reg = hyperparameters['use_reg']
    reg_beta = hyperparameters['reg_beta']
    beta1 = hyperparameters['beta1']
    beta2 = hyperparameters['beta2']
    base_learning_rate = hyperparameters["base_learning_rate"]
    use_learning_decay = hyperparameters['use_learning_decay']
    decay_steps = hyperparameters['decay_steps']  # after how many steps use the dacay
    decay_rate = hyperparameters['decay_rate']  # in what factor should we decrease the learning rate
    use_drop_out = hyperparameters['use_drop_out']
    drop_out_rate = hyperparameters['drop_out_rate']
    use_batch_norm=hyperparameters['use_batch_norm']




    inputs=features['inputs']

    z_1 = tf.matmul(inputs, tf.transpose(w_1), name='Z1')


    #if use_batch_norm:
    #    z_1 = tf.layers.batch_normalization (inputs=z_1, training=mode == tf.estimator.ModeKeys.TRAIN)
    #else:
    #    z_1 = tf.add(z_1, tf.transpose(b_1))

    a_1 = tf.nn.tanh(z_1)
    #if use_drop_out.dropout:
    #    a_1 = tf.layers.dropout(inputs=a_1, rate=drop_out_rate, training=mode == tf.estimator.ModeKeys.TRAIN)


    z_2 = tf.matmul(a_1, tf.transpose(w_2), name='Z2')
    #if use_batch_norm:
    #    z_2 = tf.layers.batch_normalization(inputs=z_2, training=mode == tf.estimator.ModeKeys.TRAIN)
    #else:
    #    z_2 = tf.add(z_2, tf.transpose(b_2))

    a_2 = tf.nn.tanh(z_2)
    #if use_drop_out.dropout:
    #    a_2 = tf.layers.dropout(inputs=a_2, rate=drop_out_rate, training=mode == tf.estimator.ModeKeys.TRAIN)

    z_3 = tf.matmul(a_2, tf.transpose(w_3), name='Z3')
    #if use_batch_norm:
    #    z_3 = tf.layers.batch_normalization(inputs=z_3, training=mode == tf.estimator.ModeKeys.TRAIN)
    #else:
    #    z_3 = tf.add(z_3, tf.transpose(b_3))

    a_3 = tf.nn.tanh(z_3)
    #if use_drop_out.dropout:
    #   a_3 = tf.layers.dropout(inputs=a_3, rate=drop_out_rate, training=mode == tf.estimator.ModeKeys.TRAIN)

    z_4 = tf.matmul(a_3, tf.transpose(w_4), name='Yhat')
    #if use_batch_norm:
    #    z_4  = tf.layers.batch_normalization (inputs=z_4, training=mode == tf.estimator.ModeKeys.TRAIN)
    #else:
    #    z_4 = tf.add(z_4, tf.transpose(b_4))

    probas = tf.nn.sigmoid(z_4, name='get_prediction')




    if use_learning_decay:
        learning_rate = tf.train.exponential_decay(base_learning_rate, global_step,
                                                   decay_steps, decay_rate, staircase=True)
    else:
        learning_rate=base_learning_rate



    if use_reg:
        reg = reg_beta * (tf.nn.l2_loss(w_1) + tf.nn.l2_loss(w_2)+ tf.nn.l2_loss(w_3))
    else:
        reg = 0


    cost_function = tf.reduce_mean(labels * -tf.log(probas) + (1 - labels) * -tf.log(1 - probas) + reg)
    optimizer= tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=beta1,
                           beta2=beta2)
    train_op = optimizer.minimize(
        loss=cost_function, global_step=global_step)

    predict = tf.to_float(probas>0.5)


    # Provide an estimator spec for `ModeKeys.PREDICT`.
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions={"probas": probas})

    eval_metric_ops = {
        'accuracy': tf.metrics.accuracy(labels, predict)
       # 'log_loss': tf.losses.logloss(labels,probas)
    }

    # Provide an estimator spec for `ModeKeys.EVAL` and `ModeKeys.TRAIN` modes.
    return tf.estimator.EstimatorSpec(
        mode=mode,
        loss=cost_function,
        train_op=train_op,
        eval_metric_ops=eval_metric_ops
       )




if __name__=='__main__':


    role = 'sagemaker_alonha'
    from sagemaker.tensorflow import TensorFlow

    hyperparams = {'use_reg': False, 'reg_beta': 0.01, 'beta1': 0.99, 'beta2': 0.999, 'base_learning_rate': 0.01,
                   'use_learning_decay': False, 'decay_steps': 10, 'decay_rate': 0.9, 'use_drop_out': False,
                   'drop_out_rate': 0.2
        , 'use_batch_norm': False, 'batch_size': 100}

    my_estimator = TensorFlow(entry_point='another_test2.py',
                              role=role,

                              train_instance_count=2,
                              checkpoint_path='s3://browsi-temp/checkpoint',

                              train_instance_type='ml.c4.xlarge',
                              training_steps=10,
                              evaluation_steps=1,
                              container_log_level=logging.DEBUG,

                              hyperparameters=hyperparams
                              )


    # use the region-specific sample data bucket

    train_data_location = 's3://sagemaker-test-patish/data/train'
    #data_location='/home/alon/Downloads/data'
    #sess=tf.Session()
    #op=train_input_fn(data_location,hyperparams)
    #x,y=sess.run(op)
    #print(x)
    #print(y)
    #x, y = sess.run(op)
    #print(x)
    #print(y)





    my_estimator.fit(train_data_location)




from tensorflow.contrib.learn.python.learn import learn_runner
from tensorflow.contrib.learn.python.learn import experiment