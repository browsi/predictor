# coding: utf-8

def balance_data(X,Y, type='under', target='is_viewed'):
    import pandas as pd
    headers=X.columns
    if type not in ('under', 'over', 'combined'):
        print('legal values for type are under, over, combined')
        return X, Y
    if type=='under':
        from imblearn.under_sampling import EditedNearestNeighbours
    #applies a nearest-neighbors algorithm and “edit” the dataset by removing samples which do not agree “enough” with their neighboorhood.
    # For each sample in the class to be under-sampled, the nearest-neighbours are computed and if the selection criterion is not fulfilled,
    # the sample is removed. all the nearest-neighbors have to belong to the same class of the sample inspected to keep it in the dataset
        enn = EditedNearestNeighbours(ratio='majority', n_neighbors=7, kind_sel='mode')
        X_res, y_res = enn.fit_sample(X, Y)

        return pd.DataFrame(data=X_res, columns=headers), pd.DataFrame(y_res, columns=[target])
    elif type=='over':
        from imblearn.over_sampling import SMOTE
    # SMOTE=  Synthetic Minority Over-Sampling Technique
    # The idea is to find the minoruty class and to generate new data. the data takes a samplle from the minority calss and finds the  K nearest neighbours from the dataset.
    # This algorithm classifies the samples of minority classes into 3 distinct groups – Security/Safe samples, Border samples, and latent nose samples. This is done by calculating the distances among samples of the minority class and samples of the training data.
    # Security samples are those data points which can improve the performance of a classifier. While on the other hand, noise are the data points which can reduce the performance of the classifier.  The ones which are difficult to categorize into any of the two are classified as border samples.
        sm = SMOTE(ratio='minority', kind='borderline1', k_neighbors=5, m_neighbors=20)
        X_res, y_res = sm.fit_sample(X, Y)
        return pd.DataFrame(data=X_res, columns=headers), pd.DataFrame(y_res, columns=[target])
    else:
        from imblearn.combine import SMOTEENN
    #We previously presented SMOTE and showed that this method can generate noisy samples by interpolating new points between marginal outliers and inliers.
    # This issue can be solved by cleaning the resulted space obtained after over-sampling.
    # What it does, essentialy is to oversample and then clean noise (undersample)
        sme = SMOTEENN(ratio='all')
        X_res, y_res = sme.fit_sample(X, Y)
        return pd.DataFrame(data=X_res, columns=headers), pd.DataFrame(y_res, columns=[target])

def standarize_numerical_colunms(DataFrame, numerical_columns, method='robust'):
    if method not in ('normal', 'robust', 'boxcox', 'minmax'):
        print('Wrong method')
        print('Avilable methods are: normal, robust, boxcox, minmax')
        return DataFrame
    from sklearn.preprocessing import RobustScaler
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.preprocessing import StandardScaler
    from scipy.stats import boxcox
    epsilon = 0.000001
    for item in numerical_columns:
        if method=='boxcox':
            if item in ('distance_from_images_array', 'distance_from_publisher_spot_array', 'distance_from_paragraphs_array', 'distance_from_social_media_widgets_array', 'distance_from_content_recommendations_array'):
                continue
            DataFrame[item]=boxcox(DataFrame[item]+epsilon)[0] #box cox the data
        elif method=='robust':
            DataFrame[item]=RobustScaler(quantile_range=(25, 75)).fit_transform(DataFrame[item].values.reshape(-1,1)) #standarize by quantiles
        elif method=='minmax':
            DataFrame[item]= MinMaxScaler(feature_range=(0,1)).fit_transform(DataFrame[item].values.reshape(-1,1))
        else:
            DataFrame[item] = StandardScaler(with_mean=True, with_std=True).fit_transform(DataFrame[item].values.reshape(-1, 1))
    return DataFrame

def write_feature_list(list):
    import config
    file = open('features.txt', 'w')
    for item in list:
        if item not in config.unwanted_columns:
            file.write("%s\n" % item)
    file.close()
    return



def get_data():
    import pandas as pd
    import config
    import numpy as np

    data = pd.read_csv(config.path_to_data)
    data=data.sample(frac=config.sample)
    data=data.dropna(how='any', axis=0)


    print('Data loaded')
    X=data[config.X_columns]
    Y = data[config.target].astype(int)

    slice_of_X_records = int(X.shape[0] * config.percent_for_train)
    slice_of_Y_records = int(Y.shape[0] * config.percent_for_train)

    X_train=X[:slice_of_X_records]
    X_test=X[slice_of_X_records:]
    Y_train=Y[:slice_of_Y_records]
    Y_test=Y[slice_of_Y_records:]

    if config.pre_standarize:
        X_train=standarize_numerical_colunms(X_train, config.X_columns,'normal')
        X_test = standarize_numerical_colunms(X_test, config.X_columns, 'normal')
        print('Data standarized')


    # Write feature list
    if config.write_feature_order:
        feature_order = X.columns.tolist()
        write_feature_list(feature_order)

    #balance data set
    if config.balance:
        X_train,Y_train=balance_data(X_train, Y_train, type=config.balance_method, target=config.target)
        Y_train=Y_train.values.reshape(-1)
        print('Data balanced')


    X_train = X_train.values
    Y_train = np.eye(config.number_of_classes)[Y_train] # a cool way for one-hot encoding, i.e. if is_viewed=1 turn it into [0,1], if is_viewed=0 turn it into [1,0]

    X_test = X_test.values
    Y_test = np.eye(config.number_of_classes)[Y_test]






    return X_train,Y_train, X_test, Y_test
