# coding: utf-8
from load_predictor import get_combined_model, get_model, test_model
import numpy as np
import logging
from logging.config import fileConfig
from os import path

log_file_path = path.join(path.dirname(path.abspath(__file__)), 'log_config.ini')
fileConfig(log_file_path)
logger = logging.getLogger(__name__)
logging.getLogger('boto').setLevel(logging.WARNING)
logging.getLogger('botocore').setLevel(logging.WARNING)
logging.getLogger('s3transfer').setLevel(logging.WARNING)
logging.getLogger('xgboost').setLevel(logging.WARNING)

def unify_data(site_key, dataset_type):
    import pandas as pd
    import os
    from os.path import join

    cwd = os.getcwd()
    file_list_names = os.listdir(join(cwd, 'spark_data', site_key, dataset_type))
    file_list = list(map(lambda x: join(cwd, 'spark_data', site_key, dataset_type, x), file_list_names))
    pandas_data_frames = []
    total_size_read = 0
    for file_path in file_list:
        file_size = os.path.getsize(file_path)
        if file_size > 1:
            logger.info('Reading file [size:{}] {}'.format(file_size, file_path))
            pandas_data_frames.append(pd.read_csv(file_path, index_col=False))
            total_size_read += file_size
            if total_size_read > 9e9:
                break
        else:
            logger.info('Skipping empty file {}'.format(file_path))
    data = pd.concat(pandas_data_frames, ignore_index=True, axis=0)
    return data


def unlabel_data(data, target='is_viewed'):
    X = data.ix[:, data.columns != target]
    Y = data[target]
    return X, Y


def get_s3_client(region, access_key, secret_key):
    import boto3
    return boto3.client('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_key, region_name=region)


def _upload_file(client, file_name, bucket, s3_path=None):
    if s3_path is None:
        logger.info("Uploading file to s3://{}/{}".format(bucket, file_name))
        client.upload_file(Bucket=bucket, Key=file_name, Filename=file_name)
    else:
        logger.info("Uploading file to s3://{}/{}/{}".format(bucket, s3_path, file_name))
        client.upload_file(Bucket=bucket, Key="{}/{}".format(s3_path, file_name), Filename=file_name)
        logger.info('Uploaded successfully')


def upload_features_file(X, client, bucket, s3_path=None):
    try:
        features_list = X.columns.tolist()
        file_name = 'features.txt'
        file_handler = open(file_name, 'w')
        for item in features_list:
            file_handler.write("%s\n" % item)
        file_handler.close()
        _upload_file(client,file_name,bucket,s3_path)
        logger.info('Uploaded successfully')
    except:
        logger.exception('Failed to upload features list to S3')
        raise


def upload_classifier(clf, client, bucket, model_type, s3_path=None):
    from sklearn.externals import joblib
    try:
        file_name = "{}.pkl".format(model_type)
        joblib.dump(clf, file_name)
        _upload_file(client,file_name,bucket,s3_path)
    except:
        logger.info('Failed to dump or upload model to S3...')


def upload_logs(client, bucket, s3_path=None):
    from os import path
    logger.info('Uploading log file to S3')
    file_handler = next(x for x in logging.getLoggerClass().root.handlers if x.__class__ == logging.FileHandler)
    log_file_name = path.relpath(file_handler.baseFilename)
    _upload_file(client, log_file_name, bucket, s3_path)


def save_data_csv(train_data, test_data, path_format):
    logger.info('Saving train data...')
    train_data.to_csv(path_format.format('train'), index=False)
    logger.info('Train data saved.')
    logger.info('Saving test data...')
    test_data.to_csv(path_format.format('test'), index=False)
    logger.info('Test data saved.')


if __name__ == '__main__':
    import automation_config as conf
    import argparse
    logger.info('Parsing arguments')

    parser = argparse.ArgumentParser()
    parser.add_argument('site_key')
    args = parser.parse_args()
    site_key = args.site_key

    s3_client = get_s3_client(conf.s3_region, conf.s3_access_key, conf.s3_secret_key)
    s3_target_path = 'automation/models/site_key={}'.format(site_key)
    target_bucket = conf.bucket_for_pkl
    try:
        train_df = unify_data(site_key, 'train')
        test_df = unify_data(site_key, 'test')
        logger.info('Data loaded')
        if conf.save_big_csv:
            save_data_csv(train_df, test_df, '/home/ubuntu/{}.csv')
        logger.info('Unlabeling data')
        X_train, Y_train = unlabel_data(train_df)
        X_test, Y_test = unlabel_data(test_df)
        X_validation, Y_validation = None, None
        logger.info('Training model...')
        if conf.cal:
            clf, clf2, probas, Y_test = get_combined_model(X_train, Y_train, X_test, Y_test, site_key)
            logger.info("Training finished")
            upload_classifier(clf, s3_client, target_bucket, 'rf', s3_target_path)
            upload_classifier(clf2, s3_client, target_bucket, 'lr', s3_target_path)
            upload_features_file(X_train, s3_client, target_bucket, s3_target_path)
            _upload_file(s3_client,"Reliabiliy_curve_with_calbiration.png", target_bucket, s3_target_path)

        else:
            clf = get_model(X_train, Y_train)
            logger.info("Training finished")
            if not conf.calibrate and conf.model == 1:
                data_frame = np.std(X_test, axis=0) / np.std(X_test, axis=0)
                logger.info((clf.feature_importances_ * data_frame).sort_values(ascending=False))

            probas = test_model(clf, X_validation, Y_validation, X_test, Y_test, site_key, validate=False)
            if conf.push_model:
                upload_classifier(clf, s3_client, target_bucket, 'rf', s3_target_path)
                upload_features_file(X_train, s3_client, target_bucket, s3_target_path)
    except Exception as e:
        logger.exception('Something went wrong...')
    upload_logs(s3_client, target_bucket, s3_target_path)
