# coding: utf-8

AWS_ACCESS_KEY_ID = 'AKIAIBXXAWHRRBO2KHYA'
AWS_SECRET_ACCESS_KEY = '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'
REGION_NAME = 'us-west-2'

# Get data from Athena aggregation #
bucket = 'browsi-viewability-prediction'
key = 'training_data.csv'
path='training_data.csv'
# if we already manualy put this CSV in the local path
file_name='/home/ubuntu/data/gravidanzaonline.csv'
site_key='gravidanzaonline'
# how do get the data? from local csv or from S3
loading_method='manual'

#is data saved in splitted files in S3
split_data=False
is_train=True
prefix_train='data/site_key=walla/model=2018_01_23_09_02/dataset=train'
prefix_test='data/site_key=walla/model=2018_01_23_08_26/dataset=test'

#page structure dictionary
import collections
d=collections.defaultdict(dict)

#parameteres for parsing
categorical_columns=['geo_location', 'traffic_source', 'browser', 'Hour_category']
element_list=['images_array','publisher_spot_array', 'paragraphs_array', 'social_media_widgets_array', 'content_recommendations_array', 'browsi_spots_array']
num_of_element_list=['num_of_images', 'num_of_paragraphs', 'num_of_social_media_widgets', 'num_of_content_recommendations','num_of_publisher_spots']
numerical_columns=['stopping_rate_user', 'avg_velocity_user',
       'avg_scroll_depth_user', 'avg_time_on_page_user',
       'avg_page_load_time_user', 'avg_scroll_depth_url',
       'avg_time_on_page_url', 'avg_page_load_time_url',
       'velocity_up_to_500', 'percent_from_time_on_page_up_to_500', 'stopping_rate_up_to_500',
       'velocity_500_to_1000', 'percent_from_time_on_page_500_to_1000',
       'stopping_rate_500_to_1000', 'velocity_1000_to_1500',
       'percent_from_time_on_page_1000_to_1500', 'stopping_rate_1000_to_1500',
       'velocity_1500_to_2000', 'percent_from_time_on_page_1500_to_2000',
       'stopping_rate_1500_to_2000', 'velocity_2000_to_2500',
       'percent_from_time_on_page_2000_to_2500', 'stopping_rate_2000_to_2500',
       'velocity_2500_to_3000', 'percent_from_time_on_page_2500_to_3000',
       'stopping_rate_2500_to_3000', 'velocity_3000_to_3500',
       'percent_from_time_on_page_3000_to_3500', 'stopping_rate_3000_to_3500',
       'velocity_3500_to_4000', 'percent_from_time_on_page_3500_to_4000',
       'stopping_rate_3500_to_4000', 'velocity_4000_to_4500',
       'percent_from_time_on_page_4000_to_4500', 'stopping_rate_4000_to_4500',
       'velocity_4500_and_higher', 'percent_from_time_on_page_4500_and_higher',
       'stopping_rate_4500_and_higher', 'num_of_images', 'num_of_paragraphs',
       'num_of_social_media_widgets', 'num_of_content_recommendations',
       'num_of_publisher_spots', 'is_lazy_loaded',
       'distance_from_images_array', 'distance_from_publisher_spot_array',
       'distance_from_paragraphs_array',
       'distance_from_social_media_widgets_array',
       'distance_from_content_recommendations_array', 'ad_location', 'hours_since_published', 'viewability_per_user', 'viewability_per_url']
velocity_columns=['avg_velocity_user',      'velocity_up_to_500',
                  'velocity_500_to_1000',   'velocity_1000_to_1500',
                  'velocity_1500_to_2000',  'velocity_2000_to_2500',
                  'velocity_2500_to_3000',  'velocity_3000_to_3500',
                  'velocity_3500_to_4000',  'velocity_4000_to_4500',
                  'velocity_4500_and_higher']

drop_column_list=['url', 'user_id', 'page_view_id', 'ad_index', 'now', 'published_date','Day', 'Hour',  'Weekday']

#sample?
sample=False
sample_size=300
# include validation set as well?
validate=False

#standartize/PCA/ balance data?
standartize=False
PCA=False
balance=False

#grid serach params
grid_search=False
grid_serach_classifier='xgboost'
grid_serach_params={'xgboost': {'objective':['binary:logistic'], 'silent':[False], 'missing':[None], 'nthread': [-1], 'n_estimators':[10], 'learning_rate':[0.01],
                                             'max_depth':[3], 'gamma': [50], 'colsample_bytree': [0.8], 'subsample':[0.9], 'reg_lambda':[1],  'min_child_weight':[0.01,0.1,0.5]
                                           , 'max_delta_step':[5], 'scale_pos_weight': [0.5,0.7,0.9,1,1.1,1.2,1.3,1.4,1.5,1.7,2]},
             'knn':  {'n_neighbors': [3,5,10,30,50,100,1000,2000], 'weights': ['distance', 'uniform']},
             'naive_bayes':  {'priors':[None]},
               'rf': {'oob_score': [True] , 'bootstrap':[True], 'criterion': ['gini'], 'n_estimators':[1000], 'max_features': [None,'sqrt'], 'max_depth': [3,5,10], 'max_leaf_nodes': [10,100,1000], 'min_impurity_decrease': [0.01,0.05,0.1],
                      'min_samples_leaf':[0.01,0.05, 0.1], 'min_samples_split': [0.01,0.05,0.1,0.2], 'class_weight':['balanced'],   'random_state':[None], 'verbose':[0]},
             'xrf': {'oob_score': [True] , 'bootstrap':[True], 'criterion': ['gini'], 'n_estimators':[1000], 'max_features': [None,'sqrt'], 'max_depth': [3,5,10], 'max_leaf_nodes': [10,100,1000], 'min_impurity_decrease': [0.01,0.05,0.1],
                      'min_samples_leaf':[0.01,0.05, 0.1], 'min_samples_split': [0.01,0.05,0.1,0.2], 'class_weight':['balanced'],   'random_state':[None], 'verbose':[0]},
             'svc': {'C': [0.1,1,10] , 'kernel':['rbf', 'linear'], 'gamma': [0.001, 0.1,1,10], 'probability':[True], 'shrinking': [True], 'tol': [0.001],
                              'class_weight':['balanced'], 'max_iter':[10000],  'random_state':[None]},
             'lr': {'penalty':['l2'], 'C': [0.001,0.01,0.1,1,10], 'class_weight': ['balanced'], 'max_iter':[10000], 'n_jobs':[-1], 'solver':['saga']}}


#params for model 1: XGboost 2: KNN 3: Naive bayes 4: random forest cost sensitive 5: regular RF 6: extreme RF 7: SVC 8: LR
params_dict={1: {'objective':'binary:logistic', 'silent':True, 'missing':None, 'nthread': -1, 'n_estimators':800, 'learning_rate':0.001,
                                             'max_depth':10, 'gamma':0.005, 'colsample_bytree': 0.7, 'subsample':0.9, 'reg_lambda':0.01,  'min_child_weight':0.01, 'max_delta_step':5000, 'scale_pos_weight':1},
             2:  {'n_neighbors': 3, 'weights': 'distance'},
             3:  {'alpha':1, 'fit_prior': True, 'binarize': None},
             4:  {'n_estimators':100, 'combination':'majority_bmr', 'max_features':None, 'n_jobs':-1, 'pruned':True,
                                          'costs':[1,10,0,0]},
             5: {'oob_score': True , 'bootstrap':'True', 'criterion': 'gini', 'n_estimators':500, 'max_features': None, 'max_depth': 10, 'max_leaf_nodes': 500, 'min_impurity_decrease': 0.15,
        'min_samples_leaf':0.05, 'min_samples_split': 0.05, 'class_weight':{0:1.055, 1: 1},   'random_state':None, 'verbose':1},
             6: {'oob_score': True , 'bootstrap':'True', 'criterion': 'entropy', 'n_estimators':1000, 'max_features': None, 'max_depth': 10, 'max_leaf_nodes': 200, 'min_impurity_decrease': 0.45,
        'min_samples_leaf':0.01, 'min_samples_split': 0.01, 'class_weight':'balanced',   'random_state':None},
             7: {'C': 10 , 'kernel':'rbf', 'gamma': 0.1, 'probability':True, 'shrinking': True, 'tol': 0.001,
                              'class_weight':{0:1, 1:1}, 'max_iter':5000,  'random_state':None},
             8: {'penalty':'l2', 'C': 0.2, 'class_weight': 'balanced', 'max_iter':100000, 'n_jobs':-1, 'solver':'saga'}}
model=1

#calibrate model?
calibrate=False #Depreciated keep False#
calibration_method='isotonic' #Depreciated

#New calibration params
cal=True
cal_method='platt'
second_stage_class_weight={0:1, 1: 1}
intercept=True
manual_lr=False
manual_coef=6.3
manual_intercept=-280
#params for trained model
bucket_for_pkl='browsi-predictor-model'
key_for_pkl='rf.pkl'
key_for_feature_list='features.txt'

#should we parse the data
parse_data=True
#should we push the model
push_model=True

#eliminate columns after parsing the data?
eliminate_cols_after_data_fetch=False
keep_cols=['viewability_per_user',  'ad_location', 'avg_scroll_depth_url']

#export results set to csv
export_results=False
