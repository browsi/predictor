
select url, user_id, page_view_id, ad_index, now, images_array, publisher_spot_array, paragraphs_array, social_media_widgets_array, content_recommendations_array, browsi_spots_array,
published_date, geo_location, traffic_source,  browser, /*browser_type, author, section,*/ is_viewed,  stopping_rate_user, avg_velocity_user,
avg_scroll_depth_user,  avg_time_on_page_user,   avg_page_load_time_user,  avg_scroll_depth_url,
avg_time_on_page_url, avg_page_load_time_url, 
velocity_up_to_500,  percent_from_time_on_page_up_to_500,stopping_rate_up_to_500,
velocity_500_to_1000,  percent_from_time_on_page_500_to_1000,stopping_rate_500_to_1000,
velocity_1000_to_1500,  percent_from_time_on_page_1000_to_1500,stopping_rate_1000_to_1500,
velocity_1500_to_2000,  percent_from_time_on_page_1500_to_2000,stopping_rate_1500_to_2000,
velocity_2000_to_2500,  percent_from_time_on_page_2000_to_2500,stopping_rate_2000_to_2500,
velocity_2500_to_3000,  percent_from_time_on_page_2500_to_3000,stopping_rate_2500_to_3000,
velocity_3000_to_3500,  percent_from_time_on_page_3000_to_3500,stopping_rate_3000_to_3500,
velocity_3500_to_4000,  percent_from_time_on_page_3500_to_4000,stopping_rate_3500_to_4000, 
velocity_4000_to_4500,  percent_from_time_on_page_4000_to_4500,stopping_rate_4000_to_4500, 
velocity_4500_and_higher,  percent_from_time_on_page_4500_and_higher,stopping_rate_4500_and_higher,
num_of_images, num_of_paragraphs, num_of_social_media_widgets, num_of_content_recommendations, num_of_publisher_spots, is_lazy_loaded,
coalesce(viewability_per_user_ad_index_0, 0) as viewability_per_user_ad_index_0 ,coalesce(viewability_per_user_ad_index_1, 0) as viewability_per_user_ad_index_1, 
coalesce(viewability_per_url_ad_index_0,  0) as viewability_per_url_ad_index_0  ,coalesce(viewability_per_url_ad_index_1, 0)  as viewability_per_url_ad_index_1
from 
(
select C.url, C.user_id, C.page_view_id, ad_index, now, images_array, publisher_spot_array, paragraphs_array, social_media_widgets_array, content_recommendations_array, browsi_spots_array,
published_date, geo_location, traffic_source,  browser, /*browser_type,*/ author, section, is_viewed,  stopping_rate_user, avg_velocity_user,
avg_scroll_depth_user,  avg_time_on_page_user,   avg_page_load_time_user,  avg_scroll_depth_url,
avg_time_on_page_url, avg_page_load_time_url, 
velocity_up_to_500,  percent_from_time_on_page_up_to_500,stopping_rate_up_to_500,
velocity_500_to_1000,  percent_from_time_on_page_500_to_1000,stopping_rate_500_to_1000,
velocity_1000_to_1500,  percent_from_time_on_page_1000_to_1500,stopping_rate_1000_to_1500,
velocity_1500_to_2000,  percent_from_time_on_page_1500_to_2000,stopping_rate_1500_to_2000,
velocity_2000_to_2500,  percent_from_time_on_page_2000_to_2500,stopping_rate_2000_to_2500,
velocity_2500_to_3000,  percent_from_time_on_page_2500_to_3000,stopping_rate_2500_to_3000,
velocity_3000_to_3500,  percent_from_time_on_page_3000_to_3500,stopping_rate_3000_to_3500,
velocity_3500_to_4000,  percent_from_time_on_page_3500_to_4000,stopping_rate_3500_to_4000, 
velocity_4000_to_4500,  percent_from_time_on_page_4000_to_4500,stopping_rate_4000_to_4500, 
velocity_4500_and_higher,  percent_from_time_on_page_4500_and_higher,stopping_rate_4500_and_higher,
num_of_images, num_of_paragraphs, num_of_social_media_widgets, num_of_content_recommendations, num_of_publisher_spots, is_lazy_loaded,
viewability_per_user_ad_index_0, viewability_per_user_ad_index_1, viewability_per_url_ad_index_0, viewability_per_url_ad_index_1, 
rank() over (partition by is_viewed order by rand() desc) as ranks
from


(


select page_view_id, url,  now, try_cast(publisher_spots_array as array(array(bigint))) as publisher_spot_array,
       try_cast(paragraphs_array as array(array(bigint))) as paragraphs_array,
       try_cast(social_media_widgets_array as array(array(bigint))) as social_media_widgets_array,
       try_cast(content_recommendations_array as array(array(bigint))) as content_recommendations_array, try_cast(images_array as array(array(bigint))) as images_array,  try_cast(browsi_spots_array as array(array(bigint))) as browsi_spots_array,
       published_date, geo_location, traffic_source,  browser, browser_type, author, section, page_length, user_id, 
       num_of_images, num_of_paragraphs, num_of_social_media_widgets, num_of_content_recommendations, num_of_publisher_spots, device
from
(
select now, url, page_view_id,   
ARRAY [ARRAY[json_array_get(json_array_get(cast(images as JSON),0),1), json_array_get(json_array_get(cast(images as JSON),0),2)],
        ARRAY[json_array_get(json_array_get(cast(images as JSON),1),1), json_array_get(json_array_get(cast(images as JSON),1),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),2),1), json_array_get(json_array_get(cast(images as JSON),2),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),3),1), json_array_get(json_array_get(cast(images as JSON),3),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),4),1), json_array_get(json_array_get(cast(images as JSON),4),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),5),1), json_array_get(json_array_get(cast(images as JSON),5),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),6),1), json_array_get(json_array_get(cast(images as JSON),6),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),7),1), json_array_get(json_array_get(cast(images as JSON),7),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),8),1), json_array_get(json_array_get(cast(images as JSON),8),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),9),1), json_array_get(json_array_get(cast(images as JSON),9),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),10),1), json_array_get(json_array_get(cast(images as JSON),10),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),11),1), json_array_get(json_array_get(cast(images as JSON),11),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),12),1), json_array_get(json_array_get(cast(images as JSON),12),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),13),1), json_array_get(json_array_get(cast(images as JSON),13),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),14),1), json_array_get(json_array_get(cast(images as JSON),14),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),15),1), json_array_get(json_array_get(cast(images as JSON),15),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),16),1), json_array_get(json_array_get(cast(images as JSON),16),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),17),1), json_array_get(json_array_get(cast(images as JSON),17),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),18),1), json_array_get(json_array_get(cast(images as JSON),18),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),19),1), json_array_get(json_array_get(cast(images as JSON),19),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),20),1), json_array_get(json_array_get(cast(images as JSON),20),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),21),1), json_array_get(json_array_get(cast(images as JSON),21),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),22),1), json_array_get(json_array_get(cast(images as JSON),22),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),23),1), json_array_get(json_array_get(cast(images as JSON),23),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),24),1), json_array_get(json_array_get(cast(images as JSON),24),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),25),1), json_array_get(json_array_get(cast(images as JSON),25),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),26),1), json_array_get(json_array_get(cast(images as JSON),26),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),27),1), json_array_get(json_array_get(cast(images as JSON),27),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),28),1), json_array_get(json_array_get(cast(images as JSON),28),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),29),1), json_array_get(json_array_get(cast(images as JSON),29),2)]]  as images_array,
 

	
 ARRAY [ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),0),1),json_array_get(json_array_get(cast(publisher_spots as JSON),0),3)],
        ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),1),1),json_array_get(json_array_get(cast(publisher_spots as JSON),1),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),2),1),json_array_get(json_array_get(cast(publisher_spots as JSON),2),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),3),1),json_array_get(json_array_get(cast(publisher_spots as JSON),3),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),4),1),json_array_get(json_array_get(cast(publisher_spots as JSON),4),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),5),1),json_array_get(json_array_get(cast(publisher_spots as JSON),5),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),6),1),json_array_get(json_array_get(cast(publisher_spots as JSON),6),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),7),1),json_array_get(json_array_get(cast(publisher_spots as JSON),7),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),8),1),json_array_get(json_array_get(cast(publisher_spots as JSON),8),3)]]  as publisher_spots_array,

 
 ARRAY [ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),0),2),json_array_get(json_array_get(cast(paragraphs as JSON),0),1)],
        ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),1),2),json_array_get(json_array_get(cast(paragraphs as JSON),1),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),2),2),json_array_get(json_array_get(cast(paragraphs as JSON),2),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),3),2),json_array_get(json_array_get(cast(paragraphs as JSON),3),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),4),2),json_array_get(json_array_get(cast(paragraphs as JSON),4),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),5),2),json_array_get(json_array_get(cast(paragraphs as JSON),5),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),6),2),json_array_get(json_array_get(cast(paragraphs as JSON),6),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),7),2),json_array_get(json_array_get(cast(paragraphs as JSON),7),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),8),2),json_array_get(json_array_get(cast(paragraphs as JSON),8),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),9),2),json_array_get(json_array_get(cast(paragraphs as JSON),9),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),10),2),json_array_get(json_array_get(cast(paragraphs as JSON),10),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),11),2),json_array_get(json_array_get(cast(paragraphs as JSON),11),1)]] as paragraphs_array,
 
  ARRAY [ARRAY[json_array_get(json_array_get(cast(social_media_widgets as JSON),0),0),json_array_get(json_array_get(cast(social_media_widgets as JSON),0),1)],
         ARRAY[json_array_get(json_array_get(cast(social_media_widgets as JSON),1),0),json_array_get(json_array_get(cast(social_media_widgets as JSON),1),1)],
		 ARRAY[json_array_get(json_array_get(cast(social_media_widgets as JSON),2),0),json_array_get(json_array_get(cast(social_media_widgets as JSON),2),1)]] as social_media_widgets_array,
 
  ARRAY [ARRAY[json_array_get(json_array_get(cast(content_recommendations as JSON),0),0),json_array_get(json_array_get(cast(content_recommendations as JSON),0),1)], 
		 ARRAY[json_array_get(json_array_get(cast(content_recommendations as JSON),1),0),json_array_get(json_array_get(cast(content_recommendations as JSON),1),1)],
		 ARRAY[json_array_get(json_array_get(cast(content_recommendations as JSON),2),0),json_array_get(json_array_get(cast(content_recommendations as JSON),2),1)]] as content_recommendations_array, 
  
  ARRAY[ARRAY[json_array_get(json_array_get(cast(browsi_spots as JSON),0),2), json_array_get(json_array_get(cast(browsi_spots as JSON),0),1), json_array_get(json_array_get(cast(browsi_spots as JSON),0),4)], 
        ARRAY[json_array_get(json_array_get(cast(browsi_spots as JSON),1),2), json_array_get(json_array_get(cast(browsi_spots as JSON),1),1), json_array_get(json_array_get(cast(browsi_spots as JSON),1),4)],
        ARRAY[json_array_get(json_array_get(cast(browsi_spots as JSON),2),2), json_array_get(json_array_get(cast(browsi_spots as JSON),2),1), json_array_get(json_array_get(cast(browsi_spots as JSON),2),4)],
        ARRAY[json_array_get(json_array_get(cast(browsi_spots as JSON),3),2), json_array_get(json_array_get(cast(browsi_spots as JSON),3),1), json_array_get(json_array_get(cast(browsi_spots as JSON),3),4)]] as browsi_spots_array,
  

  
  json_array_get(cast(page_meta_data as JSON),3) as published_date, 
  coalesce(json_array_get(cast(page_meta_data as JSON),2), cast('NA' as json)) as author, 
  coalesce(json_array_get(cast(page_meta_data as JSON),2), cast('NA' as json)) as section, 
  case when geo_location in ('us', 'br', 'gb') then geo_location else 'ROW' end as geo_location, lower(traffic_source) as traffic_source,  page_length, 
  case when  lower(browser) like '%chrome%' then 'chrome' when lower(browser) like '%safari%' then 'safari' else 'Other_Browser' end as browser, browser_type, user_id, 
  num_of_images, num_of_paragraphs, num_of_social_media_widgets, num_of_content_recommendations, num_of_publisher_spots, device
 
from browsi_general_page_structure  
where site_key='12up' and day=26 and month=12 and json_array_get(json_array_get(cast(browsi_spots as JSON),0),2) is not NULL /*makes sure browsi spots is not empty*/   ) A 

) C

inner join

(


select url, device, A.page_view_id, A.ad_index  ,(case when concat(B.page_view_id, cast(B.ad_index as varchar)) is NULL then 0 else 1 end) as is_viewed, is_lazy_loaded
 from
(select distinct   url, device, page_view_id, ad_index, case when is_lazy_loaded=true then 1 else 0 end as  is_lazy_loaded   from demand_parquet  where site_key='12up' and day=26 and month=12 and event_type='ad_impression' and fold is not NULL and refresh_count=0) A

left outer join
 
(select distinct   page_view_id, ad_index  from demand_parquet where site_key='12up' and day=26 and month=12 and event_type='viewed_impression' and fold is not NULL and refresh_count=0) B

on A.page_view_id=B.page_view_id and A.ad_index=B.ad_index 

) D


 on C.url=D.url and C.device=D.device  
 
 inner join 
 
 
 (
select user_id, coalesce(count(stop)/cast(count(*) as double),0) as stopping_rate_user, avg(velocity) as avg_velocity_user, 
avg(case when rank_sd=1 then depth_percentage end) as avg_scroll_depth_user, 
avg(case when rank_time=1 then time_on_page end) as avg_time_on_page_user,
avg(case when rank_pl=1 then page_load_time end) as avg_page_load_time_user

from
(
select user_id, page_view_id, case when velocity>0 then velocity end as velocity, case when velocity=0 then 0 end as stop, depth_percentage, time_on_page,page_load_time, rank() over (partition by page_view_id order by time_on_page desc) as rank_time, rank() over (partition by page_view_id order by depth_percentage desc) as rank_sd, rank() over (partition by page_view_id order by page_load_time desc) as rank_pl
from engagement_parquet 
where day=26 and month=12 and year=2017 and site_key='12up' and is_benchmark_pageview=false  and depth_percentage>0 and depth_percentage<=100 and page_load_time>0 and page_load_time<=300000) A
group by user_id 
) E

on C.user_id=E.user_id

inner join
(
select user_id, case when ad_index=0 then viewability_per_user end as viewability_per_user_ad_index_0,  case when ad_index=1 then viewability_per_user end as viewability_per_user_ad_index_1
from
(select user_id, ad_index, count(distinct case when event_type='viewed_impression' then concat(page_view_id, cast(ad_index as varchar)) end)/cast(count(distinct case when event_type='ad_impression' then concat(page_view_id, cast(ad_index as varchar)) end) as double) as viewability_per_user
from demand_parquet 
where site_key='12up' and day=26 and month=12 and event_type in ('viewed_impression', 'ad_impression') and fold is not NULL and refresh_count=0
group by user_id, ad_index ) v
) V1
on E.user_id=V1.user_id
inner join 
(
select url, device, case when ad_index=0 then viewability_per_url end as viewability_per_url_ad_index_0,  case when ad_index=1 then viewability_per_url end as viewability_per_url_ad_index_1
from
(
select url, device, ad_index,  count(distinct case when event_type='viewed_impression' then concat(page_view_id, cast(ad_index as varchar)) end)/cast(count(distinct case when event_type='ad_impression' then concat(page_view_id, cast(ad_index as varchar)) end) as double) as viewability_per_url
from demand_parquet 
where site_key='12up' and day=26 and month=12 and event_type in ('viewed_impression', 'ad_impression') and fold is not NULL and refresh_count=0
group by url, device, ad_index ) v
) V2
on C.url=V2.url and C.device=V2.device

inner join

(
select url , device, avg(case when rank_sd=1 then depth_percentage end) as avg_scroll_depth_url, avg(case when rank_time=1 then time_on_page end) as avg_time_on_page_url, avg(case when rank_pl=1 then page_load_time end) as avg_page_load_time_url,
avg(case when cast(pixel_offset as double)>0 and cast(pixel_offset as double)<=500 then velocity end) as velocity_up_to_500,
coalesce(count(case when cast(pixel_offset as double)>0 and cast(pixel_offset as double)<=500 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_up_to_500,
coalesce(count(case when cast(pixel_offset as double)>0 and cast(pixel_offset as double)<=500 then stop end)/cast(count(case when cast(pixel_offset as double)>0 and cast(pixel_offset as double)<=500 then page_view_id end) as double),0) as stopping_rate_up_to_500,
avg(case when cast(pixel_offset as double)>500 and cast(pixel_offset as double)<=1000 then velocity end) as velocity_500_to_1000,
coalesce(count(case when cast(pixel_offset as double)>500 and cast(pixel_offset as double)<=1000 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_500_to_1000,
coalesce(count(case when cast(pixel_offset as double)>500 and cast(pixel_offset as double)<=1000 then stop end)/cast(count(case when cast(pixel_offset as double)>500 and cast(pixel_offset as double)<=1000 then page_view_id end) as double),0) as stopping_rate_500_to_1000,
avg(case when cast(pixel_offset as double)>1000 and cast(pixel_offset as double)<=1500 then velocity end) as velocity_1000_to_1500,
coalesce(count(case when cast(pixel_offset as double)>1000 and cast(pixel_offset as double)<=1500 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_1000_to_1500,
coalesce(count(case when cast(pixel_offset as double)>1000 and cast(pixel_offset as double)<=1500 then stop end)/cast(count(case when cast(pixel_offset as double)>1000 and cast(pixel_offset as double)<=1500 then page_view_id end) as double),0) as stopping_rate_1000_to_1500,
avg(case when cast(pixel_offset as double)>1500 and cast(pixel_offset as double)<=2000 then velocity end) as velocity_1500_to_2000,
coalesce(count(case when cast(pixel_offset as double)>1500 and cast(pixel_offset as double)<=2000 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_1500_to_2000,
coalesce(count(case when cast(pixel_offset as double)>1500 and cast(pixel_offset as double)<=2000 then stop end)/cast(count(case when cast(pixel_offset as double)>1500 and cast(pixel_offset as double)<=2000 then page_view_id end) as double),0) as stopping_rate_1500_to_2000,
avg(case when cast(pixel_offset as double)>2000 and cast(pixel_offset as double)<=2500 then velocity end) as velocity_2000_to_2500,
coalesce(count(case when cast(pixel_offset as double)>2000 and cast(pixel_offset as double)<=2500 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_2000_to_2500,
coalesce(count(case when cast(pixel_offset as double)>2000 and cast(pixel_offset as double)<=2500 then stop end)/cast(count(case when cast(pixel_offset as double)>2000 and cast(pixel_offset as double)<=2500 then page_view_id end) as double),0) as stopping_rate_2000_to_2500,
avg(case when cast(pixel_offset as double)>2500 and cast(pixel_offset as double)<=3000 then velocity end) as velocity_2500_to_3000,
coalesce(count(case when cast(pixel_offset as double)>2500 and cast(pixel_offset as double)<=3000 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_2500_to_3000,
coalesce(count(case when cast(pixel_offset as double)>2500 and cast(pixel_offset as double)<=3000 then stop end)/cast(count(case when cast(pixel_offset as double)>2500 and cast(pixel_offset as double)<=3000 then page_view_id end) as double),0) as stopping_rate_2500_to_3000,
avg(case when cast(pixel_offset as double)>3000 and cast(pixel_offset as double)<=3500 then velocity end) as velocity_3000_to_3500,
coalesce(count(case when cast(pixel_offset as double)>3000 and cast(pixel_offset as double)<=3500 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_3000_to_3500,
coalesce(count(case when cast(pixel_offset as double)>3000 and cast(pixel_offset as double)<=3500 then stop end)/cast(count(case when cast(pixel_offset as double)>3000 and cast(pixel_offset as double)<=3500 then page_view_id end) as double),0) as stopping_rate_3000_to_3500,
avg(case when cast(pixel_offset as double)>3500 and cast(pixel_offset as double)<=4000 then velocity end) as velocity_3500_to_4000,
coalesce(count(case when cast(pixel_offset as double)>3500 and cast(pixel_offset as double)<=4000 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_3500_to_4000,
coalesce(count(case when cast(pixel_offset as double)>3500 and cast(pixel_offset as double)<=4000 then stop end)/cast(count(case when cast(pixel_offset as double)>3500 and cast(pixel_offset as double)<=4000 then page_view_id end) as double),0) as stopping_rate_3500_to_4000,
avg(case when cast(pixel_offset as double)>4000 and cast(pixel_offset as double)<=4500 then velocity end) as velocity_4000_to_4500,
coalesce(count(case when cast(pixel_offset as double)>4000 and cast(pixel_offset as double)<=4500 then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_4000_to_4500,
coalesce(count(case when cast(pixel_offset as double)>4000 and cast(pixel_offset as double)<=4500 then stop end)/cast(count(case when cast(pixel_offset as double)>4000 and cast(pixel_offset as double)<=4500 then page_view_id end) as double),0) as stopping_rate_4000_to_4500,
avg(case when cast(pixel_offset as double)>4500  then velocity end) as velocity_4500_and_higher,
coalesce(count(case when cast(pixel_offset as double)>4500  then page_view_id end)/cast(count(page_view_id) as double),0) as percent_from_time_on_page_4500_and_higher,
coalesce(count(case when cast(pixel_offset as double)>4500  then stop end)/cast(count(case when cast(pixel_offset as double)>4500  then page_view_id end) as double),0) as stopping_rate_4500_and_higher
 
from
(
select  url, device, page_view_id,  depth_percentage, time_on_page, pixel_offset, page_load_time, case when velocity>0 then velocity end as velocity, case when velocity=0 then 0 end as stop, 
rank() over (partition by page_view_id order by time_on_page desc) as rank_time, rank() over (partition by page_view_id order by depth_percentage desc) as rank_sd, rank() over (partition by page_view_id order by page_load_time desc) as rank_pl


from engagement_parquet 
where day=26 and month=12 and year=2017 and site_key='12up' and is_benchmark_pageview=false  and depth_percentage>0 and depth_percentage<=100 and page_load_time>0 and page_load_time<=300000) A
group by url, device
 
 ) as E2
 
on C.url=E2.url and C.device=E2.device
where  trim(url_extract_path(try(url_decode(C.url)))) not in ('', '/') 

) final
where ranks<=100000