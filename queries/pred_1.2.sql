select C.url, C.user_id, page_width,  ad_index, now, images_array, publisher_spots_array, paragraphs_array, social_media_widgets_array, content_recommendations_array,comment_sections_array, video_players_array,
num_of_images, num_of_paragraphs, num_of_social_media_widgets, num_of_content_recommendations, num_of_publisher_spots, num_of_video_players, num_of_comment_sections, density_publisher, density_total,
published_date, geo_location, traffic_source,  browser, /*browser_type,*/  is_viewed,  is_lazy_loaded, is_refreshed, offset_top,  height, avg_scroll_depth_user, 
avg_time_on_page_user, avg_page_load_time_user, avg_load_time_user, avg_velocity_user, stopping_rate_user, reverse_scrolling_user,
avg_scroll_depth_url, 
avg_time_on_page_url, 
avg_page_load_time_url,
avg_load_time_url,
avg_velocity_url,
velocity_up_to_500,
percent_from_time_on_page_up_to_500,
stopping_rate_up_to_500,
reverse_scrolling_up_to_500,
velocity_500_to_1000,
percent_from_time_on_page_500_to_1000,
stopping_rate_500_to_1000,
reverse_scrolling_500_to_1000,
velocity_1000_to_1500,
percent_from_time_on_page_1000_to_1500,
stopping_rate_1000_to_1500,
reverse_scrolling_1000_to_1500,
velocity_1500_to_2000,
percent_from_time_on_page_1500_to_2000,
stopping_rate_1500_to_2000,
reverse_scrolling_1500_to_2000,
velocity_2000_to_2500,
percent_from_time_on_page_2000_to_2500,
stopping_rate_2000_to_2500,
reverse_scrolling_2000_to_2500,
velocity_2500_to_3000,
percent_from_time_on_page_2500_to_3000,
stopping_rate_2500_to_3000,
reverse_scrolling_2500_to_3000,
velocity_3000_to_3500,
percent_from_time_on_page_3000_to_3500,
stopping_rate_3000_to_3500,
 reverse_scrolling_3000_to_3500,
velocity_3500_to_4000,
percent_from_time_on_page_3500_to_4000,
stopping_rate_3500_to_4000,
reverse_scrolling_3500_to_4000,
velocity_4000_to_4500,
percent_from_time_on_page_4000_to_4500,
stopping_rate_4000_to_4500,
reverse_scrolling_4000_to_4500,
velocity_4500_to_5000,
percent_from_time_on_page_4500_to_5000,
stopping_rate_4500_to_5000,
reverse_scrolling_4500_to_5000,
coalesce(viewability_per_user_ad_index_0, 0) as viewability_per_user_ad_index_0,
coalesce(viewability_per_user_ad_index_1, 0) as viewability_per_user_ad_index_1,
coalesce(viewability_per_url_ad_index_0, 0) as viewability_per_url_ad_index_0,
coalesce(viewability_per_url_ad_index_1, 0) as viewability_per_url_ad_index_1


from
(


select now, url, 
try_cast(ARRAY [ARRAY[json_array_get(json_array_get(cast(images as JSON),0),1), json_array_get(json_array_get(cast(images as JSON),0),2)],
        ARRAY[json_array_get(json_array_get(cast(images as JSON),1),1), json_array_get(json_array_get(cast(images as JSON),1),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),2),1), json_array_get(json_array_get(cast(images as JSON),2),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),3),1), json_array_get(json_array_get(cast(images as JSON),3),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),4),1), json_array_get(json_array_get(cast(images as JSON),4),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),5),1), json_array_get(json_array_get(cast(images as JSON),5),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),6),1), json_array_get(json_array_get(cast(images as JSON),6),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),7),1), json_array_get(json_array_get(cast(images as JSON),7),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),8),1), json_array_get(json_array_get(cast(images as JSON),8),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),9),1), json_array_get(json_array_get(cast(images as JSON),9),2)],
		ARRAY[json_array_get(json_array_get(cast(images as JSON),10),1), json_array_get(json_array_get(cast(images as JSON),10),2)]]  as array(array(bigint)))  as images_array,
 

	
 try_cast(ARRAY [ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),0),1),json_array_get(json_array_get(cast(publisher_spots as JSON),0),3)],
        ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),1),1),json_array_get(json_array_get(cast(publisher_spots as JSON),1),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),2),1),json_array_get(json_array_get(cast(publisher_spots as JSON),2),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),3),1),json_array_get(json_array_get(cast(publisher_spots as JSON),3),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),4),1),json_array_get(json_array_get(cast(publisher_spots as JSON),4),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),5),1),json_array_get(json_array_get(cast(publisher_spots as JSON),5),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),6),1),json_array_get(json_array_get(cast(publisher_spots as JSON),6),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),7),1),json_array_get(json_array_get(cast(publisher_spots as JSON),7),3)],
		ARRAY[json_array_get(json_array_get(cast(publisher_spots as JSON),8),1),json_array_get(json_array_get(cast(publisher_spots as JSON),8),3)]]   as array(array(bigint))) as publisher_spots_array,

 
 try_cast(ARRAY [ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),0),2),json_array_get(json_array_get(cast(paragraphs as JSON),0),1)],
        ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),1),2),json_array_get(json_array_get(cast(paragraphs as JSON),1),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),2),2),json_array_get(json_array_get(cast(paragraphs as JSON),2),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),3),2),json_array_get(json_array_get(cast(paragraphs as JSON),3),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),4),2),json_array_get(json_array_get(cast(paragraphs as JSON),4),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),5),2),json_array_get(json_array_get(cast(paragraphs as JSON),5),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),6),2),json_array_get(json_array_get(cast(paragraphs as JSON),6),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),7),2),json_array_get(json_array_get(cast(paragraphs as JSON),7),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),8),2),json_array_get(json_array_get(cast(paragraphs as JSON),8),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),9),2),json_array_get(json_array_get(cast(paragraphs as JSON),9),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),10),2),json_array_get(json_array_get(cast(paragraphs as JSON),10),1)],
		ARRAY[json_array_get(json_array_get(cast(paragraphs as JSON),11),2),json_array_get(json_array_get(cast(paragraphs as JSON),11),1)]]  as array(array(bigint))) as paragraphs_array,
 
  try_cast(ARRAY [ARRAY[json_array_get(json_array_get(cast(social_media_widgets as JSON),0),0),json_array_get(json_array_get(cast(social_media_widgets as JSON),0),1)],
         ARRAY[json_array_get(json_array_get(cast(social_media_widgets as JSON),1),0),json_array_get(json_array_get(cast(social_media_widgets as JSON),1),1)],
		 ARRAY[json_array_get(json_array_get(cast(social_media_widgets as JSON),2),0),json_array_get(json_array_get(cast(social_media_widgets as JSON),2),1)]]  as array(array(bigint))) as social_media_widgets_array,
 
  try_cast(ARRAY [ARRAY[json_array_get(json_array_get(cast(content_recommendations as JSON),0),0),json_array_get(json_array_get(cast(content_recommendations as JSON),0),1)], 
		 ARRAY[json_array_get(json_array_get(cast(content_recommendations as JSON),1),0),json_array_get(json_array_get(cast(content_recommendations as JSON),1),1)],
		 ARRAY[json_array_get(json_array_get(cast(content_recommendations as JSON),2),0),json_array_get(json_array_get(cast(content_recommendations as JSON),2),1)]]  as array(array(bigint))) as content_recommendations_array, 
  
 

  try_cast(ARRAY[ARRAY[json_array_get(json_array_get(cast(comment_sections as JSON),0),1), json_array_get(json_array_get(cast(comment_sections as JSON),0),3)], 
        ARRAY[json_array_get(json_array_get(cast(comment_sections as JSON),1),1), json_array_get(json_array_get(cast(comment_sections as JSON),1),3)]]  as array(array(bigint))) as comment_sections_array,
  
  try_cast(ARRAY[ARRAY[json_array_get(json_array_get(cast(video_players as JSON),0),1), json_array_get(json_array_get(cast(video_players as JSON),0),2)], 
        ARRAY[json_array_get(json_array_get(cast(video_players as JSON),1),1), json_array_get(json_array_get(cast(video_players as JSON),1),2)]]  as array(array(bigint))) as video_players_array,
  
  json_array_get(cast(page_meta_data as JSON),3) as published_date, 
  case when geo_location in ('us', 'br', 'gb') then geo_location else 'ROW' end as geo_location, lower(traffic_source) as traffic_source,  page_length, 
  case when  lower(browser) like '%chrome%' then 'chrome' when lower(browser) like '%safari%' then 'safari' else 'Other_Browser' end as browser, browser_type, user_id, page_width,
  num_of_images, num_of_paragraphs, num_of_social_media_widgets, num_of_content_recommendations, num_of_publisher_spots, num_of_video_players, num_of_comment_sections, density_publisher, density_total
 
from browsi_general_page_structure  
where site_key='90min' and month=1 and day>=22 and event_type='page_structure' and is_benchmark_pageview=false

) C

inner join

(


select url,  A.page_view_id, A.ad_index  ,(case when B.page_view_id is NULL then 0 else 1 end) as is_viewed, is_lazy_loaded, offset_top,  height, case when  A.refresh_count>=1 then 1 else 0 end as is_refreshed
 from
 
(select distinct   url,  page_view_id, ad_index, coalesce(refresh_count,0) as refresh_count, case when is_lazy_loaded=true then 1 else 0 end as  is_lazy_loaded, offset_top_d as offset_top, coalesce(try_cast(height as bigint),250) as height  
 from demand_parquet  where site_key='90min' and month=1 and day>=22 and event_type='ad_impression' and fold is not NULL ) A

left outer join
 
(select distinct   page_view_id, ad_index, coalesce(refresh_count,0) as refresh_count 
 from demand_parquet where site_key='90min' and month=1 and day>=22 and event_type='viewed_impression' and fold is not NULL ) B

on A.page_view_id=B.page_view_id and A.ad_index=B.ad_index  and A.refresh_count=B.refresh_count


) D


 on C.url=D.url 
 
 inner join 
 
 
 (
select user_id, avg(distinct scroll_depth_max) as avg_scroll_depth_user, 
avg(distinct time_on_page_max) as avg_time_on_page_user, 
avg(distinct page_load_time_max) as avg_page_load_time_user,
avg(distinct load_time_max) as avg_load_time_user,
avg(velocity) as avg_velocity_user,
coalesce(sum(case when velocity=0 then time_delta end)/cast(sum(time_delta) as double),-1) as stopping_rate_user,
coalesce(sum(case when pixel_delta<0 then time_delta end)/cast(sum(time_delta) as double),-1) as reverse_scrolling_user
	 

from
(
select  user_id, page_view_id,  depth_percentage, time_on_page,  time_delta , pixel_delta,
page_load_time, velocity,
max(time_on_page+rand()/10000) over (partition by page_view_id) as time_on_page_max, 
max(depth_percentage+rand()/10000) over (partition by page_view_id) as scroll_depth_max, 
coalesce(max(page_load_time+rand()/10000) over (partition by page_view_id), -1) as page_load_time_max,
coalesce(max(load_time+rand()/10000) over (partition by page_view_id), -1) as load_time_max

from engagement_parquet 
where month=1 and day>=22 and site_key='90min' and is_benchmark_pageview=false  and depth_percentage>0 and depth_percentage<=100 ) A
group by user_id
) E

on C.user_id=E.user_id

inner join
(

select user_id, 
 count(distinct case when event_type='viewed_impression' and ad_index=0  then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0) as varchar)) end)/
 cast(count(distinct case when event_type='ad_impression' and ad_index=0 then concat(page_view_id, cast(ad_index as varchar),  cast(coalesce(refresh_count,0)  as varchar)) end) as double) as viewability_per_user_ad_index_0,

 count(distinct case when event_type='viewed_impression' and ad_index=1  then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0)  as varchar)) end)/
 cast(count(distinct case when event_type='ad_impression' and ad_index=1 then concat(page_view_id, cast(ad_index as varchar),  cast(coalesce(refresh_count,0)  as varchar)) end) as double) as viewability_per_user_ad_index_1

 
 from demand_parquet 
where site_key='90min' and month=1 and day>=22 and event_type in ('viewed_impression', 'ad_impression') and fold is not NULL and ad_index in (0,1)  
group by user_id 

) V1
on E.user_id=V1.user_id
inner join 
(
select url, 
 count(distinct case when event_type='viewed_impression' and ad_index=0  then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0) as varchar)) end)/
 cast(count(distinct case when event_type='ad_impression' and ad_index=0 then concat(page_view_id, cast(ad_index as varchar),  cast(coalesce(refresh_count,0)  as varchar)) end) as double) as viewability_per_url_ad_index_0,

 count(distinct case when event_type='viewed_impression' and ad_index=1  then concat(page_view_id, cast(ad_index as varchar), cast(coalesce(refresh_count,0)  as varchar)) end)/
 cast(count(distinct case when event_type='ad_impression' and ad_index=1 then concat(page_view_id, cast(ad_index as varchar),  cast(coalesce(refresh_count,0)  as varchar)) end) as double) as viewability_per_url_ad_index_1

 
 from demand_parquet 
where site_key='90min' and month=1 and day>=22 and event_type in ('viewed_impression', 'ad_impression') and fold is not NULL and ad_index in (0,1)  
group by url
) V2
on C.url=V2.url 

inner join

(
select url ,  
avg(distinct scroll_depth_max) as avg_scroll_depth_url, 
avg(distinct time_on_page_max) as avg_time_on_page_url, 
avg(distinct page_load_time_max) as avg_page_load_time_url,
avg(distinct load_time_max) as avg_load_time_url,
avg(velocity) as avg_velocity_url,
coalesce(avg(case when pixel_offset>=0 and pixel_offset<=500 and velocity>0 then velocity end),-1) as velocity_up_to_500,
coalesce(sum(case when pixel_offset>=0 and pixel_offset<=500 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_up_to_500,
coalesce(sum(case when pixel_offset>=0 and pixel_offset<=500 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>=0 and pixel_offset<=500 then time_delta end) as double),-1) as stopping_rate_up_to_500,
coalesce(sum(case when pixel_offset>=0 and pixel_offset<=500 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>=0 and pixel_offset<=500 then time_delta end) as double),-1) as reverse_scrolling_up_to_500,

coalesce(avg(case when pixel_offset>500 and pixel_offset<=1000 and velocity>0 then velocity end), -1) as velocity_500_to_1000,
coalesce(sum(case when pixel_offset>500 and pixel_offset<=1000 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_500_to_1000,
coalesce(sum(case when pixel_offset>500 and pixel_offset<=1000 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>500 and pixel_offset<=1000 then time_delta end) as double),-1) as stopping_rate_500_to_1000,
coalesce(sum(case when pixel_offset>500 and pixel_offset<=1000 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>500 and pixel_offset<=1000 then time_delta end) as double),-1) as reverse_scrolling_500_to_1000,

coalesce(avg(case when pixel_offset>1000 and pixel_offset<=1500 and velocity>0 then velocity end) ,-1) as velocity_1000_to_1500,
coalesce(sum(case when pixel_offset>1000 and pixel_offset<=1500 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_1000_to_1500,
coalesce(sum(case when pixel_offset>1000 and pixel_offset<=1500 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>1000 and pixel_offset<=1500 then time_delta end) as double),-1) as stopping_rate_1000_to_1500,
coalesce(sum(case when pixel_offset>1000 and pixel_offset<=1500 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>1000 and pixel_offset<=1500 then time_delta end) as double),-1) as reverse_scrolling_1000_to_1500,

coalesce(avg(case when pixel_offset>1500 and pixel_offset<=2000 and velocity>0 then velocity end)  ,-1) as velocity_1500_to_2000,
coalesce(sum(case when pixel_offset>1500 and pixel_offset<=2000 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_1500_to_2000,
coalesce(sum(case when pixel_offset>1500 and pixel_offset<=2000 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>1500 and pixel_offset<=2000 then time_delta end) as double),-1) as stopping_rate_1500_to_2000,
coalesce(sum(case when pixel_offset>1500 and pixel_offset<=2000 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>1500 and pixel_offset<=2000 then time_delta end) as double),-1) as reverse_scrolling_1500_to_2000,

coalesce(avg(case when pixel_offset>2000 and pixel_offset<=2500 and velocity>0 then velocity end)  ,-1) as velocity_2000_to_2500,
coalesce(sum(case when pixel_offset>2000 and pixel_offset<=2500 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_2000_to_2500,
coalesce(sum(case when pixel_offset>2000 and pixel_offset<=2500 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>2000 and pixel_offset<=2500 then time_delta end) as double),-1) as stopping_rate_2000_to_2500,
coalesce(sum(case when pixel_offset>2000 and pixel_offset<=2500 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>2000 and pixel_offset<=2500 then time_delta end) as double),-1) as reverse_scrolling_2000_to_2500,

coalesce(avg(case when pixel_offset>2500 and pixel_offset<=3000 and velocity>0 then velocity end)  ,-1) as velocity_2500_to_3000,
coalesce(sum(case when pixel_offset>2500 and pixel_offset<=3000 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_2500_to_3000,
coalesce(sum(case when pixel_offset>2500 and pixel_offset<=3000 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>2500 and pixel_offset<=3000 then time_delta end) as double),-1) as stopping_rate_2500_to_3000,
coalesce(sum(case when pixel_offset>2500 and pixel_offset<=3000 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>2500 and pixel_offset<=3000 then time_delta end) as double),-1) as reverse_scrolling_2500_to_3000,

coalesce(avg(case when pixel_offset>3000 and pixel_offset<=3500 and velocity>0 then velocity end)  ,-1) as velocity_3000_to_3500,
coalesce(sum(case when pixel_offset>3000 and pixel_offset<=3500 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_3000_to_3500,
coalesce(sum(case when pixel_offset>3000 and pixel_offset<=3500 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>3000 and pixel_offset<=3500 then time_delta end) as double),-1) as stopping_rate_3000_to_3500,
coalesce(sum(case when pixel_offset>3000 and pixel_offset<=3500 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>3000 and pixel_offset<=3500 then time_delta end) as double),-1) as reverse_scrolling_3000_to_3500,

coalesce(avg(case when pixel_offset>3500 and pixel_offset<=4000 and velocity>0 then velocity end)  ,-1) as velocity_3500_to_4000,
coalesce(sum(case when pixel_offset>3500 and pixel_offset<=4000 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_3500_to_4000,
coalesce(sum(case when pixel_offset>3500 and pixel_offset<=4000 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>3500 and pixel_offset<=4000 then time_delta end) as double),-1) as stopping_rate_3500_to_4000,
coalesce(sum(case when pixel_offset>3500 and pixel_offset<=4000 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>3500 and pixel_offset<=4000 then time_delta end) as double),-1) as reverse_scrolling_3500_to_4000,

coalesce(avg(case when pixel_offset>4000 and pixel_offset<=4500 and velocity>0 then velocity end)  ,-1) as velocity_4000_to_4500,
coalesce(sum(case when pixel_offset>4000 and pixel_offset<=4500 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_4000_to_4500,
coalesce(sum(case when pixel_offset>4000 and pixel_offset<=4500 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>4000 and pixel_offset<=4500 then time_delta end) as double),-1) as stopping_rate_4000_to_4500,
coalesce(sum(case when pixel_offset>4000 and pixel_offset<=4500 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>4000 and pixel_offset<=4500 then time_delta end) as double),-1) as reverse_scrolling_4000_to_4500,

coalesce(avg(case when pixel_offset>4500 and pixel_offset<=5000 and velocity>0 then velocity end)  ,-1) as velocity_4500_to_5000,
coalesce(sum(case when pixel_offset>4500 and pixel_offset<=5000 then time_delta end)/cast(sum(time_delta) as double),-1) as percent_from_time_on_page_4500_to_5000,
coalesce(sum(case when pixel_offset>4500 and pixel_offset<=5000 and velocity=0 then time_delta end)/cast(sum(case when pixel_offset>4500 and pixel_offset<=5000 then time_delta end) as double),-1) as stopping_rate_4500_to_5000,
coalesce(sum(case when pixel_offset>4500 and pixel_offset<=5000 and pixel_delta<0 then time_delta end)/cast(sum(case when pixel_offset>4500 and pixel_offset<=5000 then time_delta end) as double),-1) as reverse_scrolling_4500_to_5000

from
(
select  url, page_view_id,  depth_percentage, time_on_page,
case when cast(pixel_offset as double)<cast(screen_length as double) then 0 else cast(pixel_offset as double)-cast(screen_length as double) end  as pixel_offset, 
  time_delta , pixel_delta,
page_load_time, velocity,
max(time_on_page+rand()/10000) over (partition by page_view_id) as time_on_page_max,  /*adding rand in order to avoid edge-cases where two page-view-id have the same value*/
max(depth_percentage+rand()/10000) over (partition by page_view_id) as scroll_depth_max, 
coalesce(max(page_load_time+rand()/10000) over (partition by page_view_id), -1) as page_load_time_max,
coalesce(max(load_time+rand()/10000) over (partition by page_view_id), -1) as load_time_max

from engagement_parquet 
where month=1 and day>=22 and site_key='90min' and is_benchmark_pageview=false  and depth_percentage>0 and depth_percentage<=100
) A
group by url
 
 ) as E2
 
on C.url=E2.url
where  trim(url_extract_path(try(url_decode(C.url)))) not in ('', '/')


