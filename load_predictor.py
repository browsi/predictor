# coding: utf-8
import matplotlib
import numpy as np
import logging

logger = logging.getLogger(__name__)

matplotlib.use('Agg')
def parse_data():
    import os
    import Parse_all_data
    data = Parse_all_data.get_data(method=config.loading_method)
    logger.info('data loaded')
    if config.sample:
        sample_size= config.sample_size
        data=data.sample(sample_size)
    logger.info('parsing page structue')
    data = data.apply(lambda row: Parse_all_data.parse_page_element_list(row, config.element_list), axis=1)
    data = Parse_all_data.add_missing_elements(data, config.element_list)
    data=data.dropna(subset=['browsi_spot_offset_and_height'])
    logger.info('calculating distances')
    data = data.apply(lambda row: Parse_all_data.calculate_distances(row, config.element_list), axis=1)
    logger.info('moving redundant element columns')
    data = Parse_all_data.drop_redundant_element_columns(data, config.element_list)
    logger.info('adding viewability features')
    data = Parse_all_data.add_viewability_to_features(data)
    logger.info('filling NAs')
    data = Parse_all_data.fill_na_element_columns(data, config.element_list)
    data = Parse_all_data.fill_na_num_of_element_columns(data, config.num_of_element_list)
    data = Parse_all_data.fill_empty_velocity(data, config.velocity_columns)
    logger.info('parsing time')
    data = data.apply(Parse_all_data.parse_time, axis=1, reduce=True)
    logger.info('adding dummy columns')
    logger.info('removing unwanted columns')
    data = Parse_all_data.drop_unwanted_columns(data, config.drop_column_list)
    data = Parse_all_data.add_dummy_columns(data)
    data = Parse_all_data.add_missing_dummy_columns(data)
    logger.info('unwanted column removed')
    # From now on the data manipulation is not independent between train/test/dev
    data_sets = {}
    if config.validate == True:
        train, validation, test = Parse_all_data.split_test_train(data, include_validation=True)
        data_sets['train'] = train
        data_sets['validation'] = validation
        data_sets['test'] = test
    else:
        train, test = Parse_all_data.split_test_train(data, include_validation=False)
        data_sets['train'] = train
        data_sets['test'] = test



    for key in data_sets.keys():
        data = data_sets[key]
        data = Parse_all_data.fill_na_with_mean(data, config.numerical_columns)
        logger.info('all NAs filled with mean')
        data = Parse_all_data.remove_remaining_na(data)
        logger.info('Remaining NA filtered')
        if config.standartize:
            logger.info('standarizing data')
            data = Parse_all_data.standarize_numerical_colunms(data, config.numerical_columns, method='robust')
        data_sets[key] = data

    for key in data_sets.keys():
        X, Y = Parse_all_data.split_X_Y(data_sets[key])
        if config.balance and key== 'train':
            X, Y = Parse_all_data.balance_data(X, Y, type='combined')
            logger.info('data balanced')
        if config.PCA:
            X = Parse_all_data.PCA(X)
            logger.info('PCA finished')
        data_sets[key] = Parse_all_data.recombine(X, Y)
        cwd = os.getcwd()
        data_sets[key].to_csv(cwd + '/' + 'parsed_' + key + config.site_key + '.csv', index=False)


    logger.info(data_sets)
    logger.info('saved parsed data')



def get_split_data(target='is_viewed',  is_train=True):   
    import config
    import pandas as pd 
    import boto3
    from os import listdir
    from os.path import isfile, join, getsize
    session=boto3.Session()
    s3=boto3.resource('s3', aws_access_key_id=config.AWS_ACCESS_KEY_ID, aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY,
                      region_name=config.REGION_NAME)

    bucket=s3.Bucket('browsi-predictor-model-dev')
    if is_train:
        prefix= config.prefix_train
        set='train'
    else:
        prefix= config.prefix_test
        set='test'
    objs=bucket.objects.filter(Prefix=prefix)
    path='/home/ubuntu/data/'+set+'/'
    i=1
    for obj in objs:
        key=obj.key
        #bucket.download_file(key, path+'part_'+str(i)+'_'+set+'.csv')
        logger.info('Downloaded {} out of {} files in {} set'.format(i, len(list(objs)), set))
        i=i+1
    allfiles = [join(path, f) for f in listdir(path) if isfile(join(path, f)) and getsize(join(path,f))>1]
    lst=[]
    count=1
    for f in allfiles:
        logger.info('Reading file: {}'.format(f))
        lst.append(pd.read_csv(f, index_col=False))
        count=count+1
        if count>7:
            break
    data=pd.concat([pd.read_csv(f, index_col=False) for f in allfiles], ignore_index=True, axis=0)
    #data.to_csv('/home/ubuntu/data/parsed_'+set+config.site_key+'.csv', index=False)
    logger.info('Data saved for {} set'.format(set))



def get_parsed_data(target='is_viewed'):
    import pandas as pd
    import os
    cwd=os.getcwd()
    #data=pd.read_csv('/home/ubuntu/data/sltrib_v_1.1_matan_train.csv')
    
    data = pd.read_csv(cwd + '/' +'parsed_' +'train' + config.site_key + '.csv')
    X_train = data.ix[:, data.columns != target]
    Y_train = data[target]
    if config.validate:
        data = pd.read_csv(cwd + '/' +'parsed_' +'validation' + config.site_key + '.csv')
        X_validation = data.ix[:, data.columns != target]
        Y_validation = data[target]

    else:
        X_validation = None
        Y_validation = None


    data=pd.read_csv(cwd + '/' +'parsed_' +'test' + config.site_key + '.csv')
    #data=pd.read_csv('/home/ubuntu/data/sltrib_v_1.1_matan_test.csv')
    
    X_test = data.ix[:, data.columns != target]
    Y_test = data[target]

    return X_train, Y_train, X_validation, Y_validation, X_test, Y_test

def eliminate_columns_after_data_fetch(X_train, Y_train, X_validation, Y_validation, X_test, Y_test):
    if config.validate:
        X_train=X_train[config.keep_cols]
        Y_train=Y_train
        X_validation=X_validation[config.keep_cols]
        Y_validation=Y_validation
        X_test=X_test[config.keep_cols]
        Y_test=Y_test
    else:
        X_train=X_train[config.keep_cols]
        Y_train=Y_train
        X_validation = None
        Y_validation = None
        X_test=X_test[config.keep_cols]
        Y_test=Y_test
    return  X_train, Y_train, X_validation, Y_validation, X_test, Y_test


def get_model(X,Y):
    import config
    model_num= config.model
    parameters= config.params_dict[model_num]
    import Create_classifiers
    if model_num==1:
        clf = Create_classifiers.GardientBoostedRandomForest(X, Y, parameters)
    elif model_num==2:
        clf= Create_classifiers.KNN(X, Y, parameters)
    elif model_num==3 and config.PCA:
        clf, clf_redundant = Create_classifiers.NaiveBayes(X, Y, [], X.columns)
    elif model_num==4:
        clf= Create_classifiers.CostSensitiveRandomForest(X, Y, parameters)
    elif model_num==5:
        clf= Create_classifiers.RandomForest(X, Y, parameters)
    elif model_num==6:
        clf= Create_classifiers.ExtremeRandomForest(X, Y, parameters)
    elif model_num==7:
        clf= Create_classifiers.SVC(X, Y, parameters)
    elif model_num==8:
        clf= Create_classifiers.Logistic_Regression(X, Y, parameters)
    return clf


def get_combined_model(X_train, Y_train, X_test, Y_test, site_key):
    import config
    import numpy as np
    model_num=1
    parameters= config.params_dict[model_num]
    import Create_classifiers
    xgb = Create_classifiers.GardientBoostedRandomForest(X_train, Y_train, parameters)
    Y_predict=xgb.predict(X_test)
    Y_proba=xgb.predict_proba(X_test)[:,1]
    new_data=np.column_stack((Y_proba,Y_test))
    #np.savetxt('first_stage_'+config.site_key+'.csv',new_data, delimiter=",")
    #fix outliers
    new_data[:,1]=np.where(((new_data[:,0]>=0.7) & (new_data[:,1]==0)),1,new_data[:,1])
    new_data[:,1]=np.where(((new_data[:,0]<=0.3) & (new_data[:,1]==1)),0,new_data[:,1])
    new_data_train=new_data[:int(0.66*new_data.shape[0]),:]
    new_data_test=new_data[int(0.66*new_data.shape[0]):,:]


    method= config.cal_method

    if method=='ir':
        from sklearn.isotonic import IsotonicRegression as IR
        lr = IR(out_of_bounds='clip')
        lr.fit(new_data_train[:, 0], new_data_train[:, 1])
        p_calibrated = lr.transform(new_data_test[:, 0])
        pr_calibrated = np.where(p_calibrated>=0.5,1,0)

    elif method=='platt':

        from sklearn.linear_model import LogisticRegression as LR
        lr = LR(class_weight=config.second_stage_class_weight, fit_intercept=config.intercept)
        lr.fit(new_data_train[:,0].reshape( -1, 1 ), new_data_train[:,1])
        if config.manual_lr:
            lr.coef_=np.array([[config.manual_coef]])
            lr.intercept_= config.manual_intercept
        logger.info('LR pararms are: ')
        logger.info('coef is {}'.format(lr.coef_))
        logger.info('intercept is {}'.format(lr.intercept_))
        p_calibrated = lr.predict_proba(new_data_test[:,0].reshape( -1, 1 ))[:,1]
        pr_calibrated= lr.predict(new_data_test[:,0].reshape( -1, 1 ))

    Y_proba=p_calibrated
    Y_predict=pr_calibrated
    Y_test=new_data_test[:,1]
    import Check_classifiers
    acc = Check_classifiers.accuracy(Y_test, Y_predict)
    auc = Check_classifiers.auc(Y_test, Y_proba)
    logloss = Check_classifiers.log_loss(Y_test, Y_proba)
    classification = Check_classifiers.gen_classification_report(Y_test, Y_predict)
    brier = Check_classifiers.brier_score(Y_test, Y_proba)
    #ROC = Check_classifiers.ROC_curve(Y_test, Y_proba, site_key)
    try:
        logger.info('Entering plt function')
        Check_classifiers.reliability_curve(Y_test, Y_proba,site_key)
        logger.info('Finished plt function')
        logger.info('ACC: {}'.format(acc))
        logger.info('AUC: {}'.format(auc))
        logger.info('Log Loss: {}'.format(logloss))
        logger.info('Brier Score: {}'.format(brier))
        logger.info(classification)
    except Exception as e:
        logger.exception('Error occurred')
    return xgb, lr, Y_proba,  Y_test


def push_model_to_s3(clf, key=None):
    import boto3
    import config
    from sklearn.externals import joblib
    bucket = config.bucket_for_pkl
    if key==None:
        key = config.key_for_pkl
    try:
        joblib.dump(clf, key)
        client_s3 = boto3.client('s3', aws_access_key_id=config.AWS_ACCESS_KEY_ID, aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY,
                                 region_name=config.REGION_NAME)
        client_s3.upload_file(Bucket=bucket, Key=key, Filename=key)
        logger.info('Uploaded successfuly')
    except:
        logger.info('Problem with file')

def push_features_to_s3(X):
    import boto3
    import config
    bucket = config.bucket_for_pkl
    key= config.key_for_feature_list
    try:
        list = X.columns.tolist()
        thefile = open(key, 'w')
        for item in list:
            thefile.write("%s\n" % item)

        thefile.close()
        client_s3 = boto3.client('s3', aws_access_key_id=config.AWS_ACCESS_KEY_ID, aws_secret_access_key=config.AWS_SECRET_ACCESS_KEY,
                                 region_name=config.REGION_NAME)
        client_s3.upload_file(Bucket=bucket, Key=key, Filename=key)
        logger.info('Uploaded successfuly')
    except:
        raise
        logger.info('Problem with file')

def test_model(clf, X_validation, Y_validation, X_test, Y_test, site_key, validate=False):
    import Check_classifiers
    X_data = X_validation if validate else X_test
    Y_data = Y_validation if validate else Y_test

    Y_predict = Check_classifiers.Predict(X_data, clf)
    Y_proba = Check_classifiers.Predict_proba(X_data, clf)
    acc = Check_classifiers.accuracy(X_data, Y_predict)
    auc = Check_classifiers.auc(Y_data, Y_proba)
    logloss = Check_classifiers.log_loss(Y_data, Y_proba)
    classification = Check_classifiers.gen_classification_report(Y_data, Y_predict)
    brier = Check_classifiers.brier_score(Y_data, Y_proba)
    ROC = Check_classifiers.ROC_curve(Y_data, Y_proba, site_key)
    Check_classifiers.reliability_curve(Y_data, Y_proba, site_key)

    logger.info('ACC: {}'.format(acc))
    logger.info('AUC: {}'.format(auc))
    logger.info('Log Loss: {}'.format(logloss))
    logger.info('Brier score: {}'.format(brier))
    logger.info(classification)
    return Y_proba
    
def export_results(Y, Y_proba):
    import numpy as np
    output=np.column_stack((Y_proba,Y_test))
    np.savetxt(config.site_key + "_results.csv", output, delimiter=",")


if  __name__=='__main__':
    import config
    if config.parse_data:
        parse_data()
    if config.split_data:
        get_split_data()
        exit()
    X_train, Y_train, X_validation, Y_validation, X_test, Y_test=get_parsed_data()
    
    if config.eliminate_cols_after_data_fetch:
        X_train, Y_train, X_validation, Y_validation, X_test, Y_test=eliminate_columns_after_data_fetch(X_train, Y_train, X_validation, Y_validation, X_test, Y_test)
    
    if config.grid_search:
        from Create_classifiers import GridSerach
        full_results,best_estimator,best_params,best_score=GridSerach(config.grid_serach_classifier, config.grid_serach_params, X_train, Y_train)
        logger.info('{} {}'.format(best_params, best_score))
        exit()

    if config.cal:
        clf, clf2, probas, Y_test=get_combined_model(X_train, Y_train, X_test, Y_test, config.site_key)
        if config.push_model:
            push_model_to_s3(clf)
            push_model_to_s3(clf2, 'lr.pkl')
            push_features_to_s3(X_train)
        if config.export_results:
            export_results(Y_test, probas)

    else:
        clf=get_model(X_train,Y_train)
        if not config.calibrate and config.model==1:
            data_frame=np.std(X_test, axis=0)/np.std(X_test, axis=0)
            logger.info('{}'.format((clf.feature_importances_ * data_frame).sort_values(ascending=False)))


        probas=test_model(clf, X_validation, Y_validation, X_test, Y_test, config.site_key, validate=False)
        if config.push_model:
            push_model_to_s3(clf)
            push_features_to_s3(X_train)
        if config.export_results:
            export_results(Y_test, probas)



